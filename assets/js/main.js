$(function () {
	$(".form-select2").select2();
	$('#datepicker1').datetimepicker({
		format: 'DD/MM/YYYY'
	});
	$('#datetimepicker1').datetimepicker();
	
	$("#range-time").slider({
		id: "range-slider",
		min: 7,
		max: 24,
		range: true,
		step: 1,
		value: [7, 24],
		tooltip_placement: "bottom"
	});
	
	$('[data-toggle="tooltip"]').tooltip();
	
	$('.carousel').carousel({
		interval: 3000
	});
});

/*price range*/
$('#sl2').slider();

var RGBChange = function() {
	$('#RGB').css('background', 'rgb('+r.getValue()+','+g.getValue()+','+b.getValue()+')')
};	
		
/*scroll to top*/

$(document).ready(function(){
	$(function () {
		$.scrollUp({
	        scrollName: 'scrollUp', // Element ID
	        scrollDistance: 300, // Distance from top/bottom before showing element (px)
	        scrollFrom: 'top', // 'top' or 'bottom'
	        scrollSpeed: 300, // Speed back to top (ms)
	        easingType: 'linear', // Scroll to top easing (see http://easings.net/)
	        animation: 'fade', // Fade, slide, none
	        animationSpeed: 200, // Animation in speed (ms)
	        scrollTrigger: false, // Set a custom triggering element. Can be an HTML string or jQuery object
					//scrollTarget: false, // Set a custom target element for scrolling to the top
	        scrollText: '<i class="fa fa-angle-up"></i>', // Text for element, can contain HTML
	        scrollTitle: false, // Set a custom <a> title if required.
	        scrollImg: false, // Set true to use image
	        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	        zIndex: 2147483647 // Z-Index for the overlay
		});
	});
});

