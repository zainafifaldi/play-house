/*
Navicat MySQL Data Transfer

Source Server         : _publicMySQL
Source Server Version : 50536
Source Host           : localhost:3306
Source Database       : db_playhouse

Target Server Type    : MYSQL
Target Server Version : 50536
File Encoding         : 65001

Date: 2016-02-02 16:05:03
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `t_admin`
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `ADMIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `AUTH` enum('adm','ven') NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `VENUE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ADMIN_ID`),
  KEY `VENUE_ID` (`VENUE_ID`),
  CONSTRAINT `t_admin_ibfk_1` FOREIGN KEY (`VENUE_ID`) REFERENCES `t_venue` (`VENUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_admin
-- ----------------------------
INSERT INTO `t_admin` VALUES ('1', 'ALDI ZAINAFIF', 'adm', 'aldi', 'aldi', null);
INSERT INTO `t_admin` VALUES ('2', 'WINAPAMUNGKAS RINO', 'ven', 'rino', 'rino', '1');
INSERT INTO `t_admin` VALUES ('3', 'AZIZ NURFALAH', 'ven', 'aziz', 'aziz', '2');

-- ----------------------------
-- Table structure for `t_ads`
-- ----------------------------
DROP TABLE IF EXISTS `t_ads`;
CREATE TABLE `t_ads` (
  `ADS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `HEADLINE` varchar(255) NOT NULL,
  `CONTENT` text NOT NULL,
  `LINK` varchar(255) DEFAULT NULL,
  `ACTIVE` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`ADS_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_ads
-- ----------------------------
INSERT INTO `t_ads` VALUES ('1', 'Winapamungkas', 'Dapatkan diskon winapamungkas untuk semua sesi', 'http://winapamungkas.com', '');
INSERT INTO `t_ads` VALUES ('2', 'Zainafif', 'Dapatkan juga diskon untuk web zainafif ini', 'http://aldizain.com', '');
INSERT INTO `t_ads` VALUES ('3', 'Nurfalah', 'Jangan dapatkan produk nurfalah di web ini. Click web untuk tidak mendapatkan barang.', null, '');

-- ----------------------------
-- Table structure for `t_book`
-- ----------------------------
DROP TABLE IF EXISTS `t_book`;
CREATE TABLE `t_book` (
  `BOOK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RESERVATION` varchar(20) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `SCHEDULE_ID` int(11) NOT NULL,
  `BOOK_DATE` date NOT NULL,
  `PRICE` double NOT NULL,
  `LOCK_STATE` int(1) NOT NULL DEFAULT '1' COMMENT '0: Cancel, 1: Pending (Schedule Lock), 2: Success (Schedule Lock)',
  `BOOK_TIMESTAMP` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `PAYMENT_METHOD` enum('CC','DD','WA') DEFAULT NULL,
  `PAYMENT_ID` varchar(20) DEFAULT NULL,
  `PAYMENT_BANK` varchar(32) DEFAULT NULL,
  `PAYMENT_AUTHCODE` varchar(16) DEFAULT NULL,
  `PAYMENT_MESSAGE` tinytext,
  `PAYMENT_TOKENTYPE` varchar(8) DEFAULT NULL,
  `PAYMENT_TOKEN` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`BOOK_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `SCHEDULE_ID` (`SCHEDULE_ID`),
  CONSTRAINT `t_book_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `t_user` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_book
-- ----------------------------
INSERT INTO `t_book` VALUES ('1', '00000001', '1', '13', '2016-01-15', '105000', '0', '2016-01-31 21:50:45', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('2', '00000001', '1', '14', '2016-01-15', '105000', '0', '2016-01-31 21:50:48', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('3', '00000001', '1', '15', '2016-01-15', '105000', '0', '2016-01-31 21:50:57', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('4', '00000002', '1', '19', '2016-01-15', '105000', '0', '2016-01-31 21:50:55', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('6', '00000002', '1', '20', '2016-01-15', '105000', '0', '2016-01-31 21:51:02', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('7', '00000003', '1', '21', '2016-01-15', '105000', '0', '2016-01-31 21:51:05', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('8', '00000004', '2', '69', '2016-01-31', '105000', '0', '2016-01-31 21:46:49', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('9', '00000004', '2', '70', '2016-01-31', '105000', '0', '2016-01-31 21:46:49', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('10', '00000005', '2', '95', '2016-02-01', '40000', '2', '2016-02-01 22:34:23', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('11', '00000005', '2', '96', '2016-02-01', '40000', '2', '2016-02-01 22:34:23', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('14', '00000005', '2', '97', '2016-02-01', '40000', '2', '2016-02-01 22:34:23', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('15', '00000005', '2', '98', '2016-02-01', '40000', '2', '2016-02-01 22:34:23', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('16', '00000006', '2', '99', '2016-02-01', '40000', '1', '2016-01-31 21:54:23', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('17', '00000007', '2', '13', '2016-02-01', '155000', '1', '2016-02-01 13:46:46', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('18', '00000007', '2', '14', '2016-02-01', '155000', '1', '2016-02-01 13:46:46', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('19', '00000008', '2', '15', '2016-02-01', '155000', '0', '2016-02-01 13:47:09', null, null, null, null, null, null, null);
INSERT INTO `t_book` VALUES ('20', '00000008', '2', '16', '2016-02-01', '155000', '0', '2016-02-01 13:47:09', null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `t_court`
-- ----------------------------
DROP TABLE IF EXISTS `t_court`;
CREATE TABLE `t_court` (
  `NAME` varchar(255) NOT NULL,
  `VENUE_ID` int(11) NOT NULL,
  `PRICE` double NOT NULL,
  `TYPE` int(11) NOT NULL,
  `PER` char(1) NOT NULL DEFAULT '1' COMMENT '0: half hour, 1: hour, 2: two hour, 3: three hour, 4: four hour',
  `PHOTO` int(11) DEFAULT NULL,
  `OPEN_RANGE` char(255) DEFAULT NULL,
  PRIMARY KEY (`NAME`,`VENUE_ID`),
  KEY `VENUE_ID` (`VENUE_ID`),
  KEY `TYPE` (`TYPE`),
  KEY `NAME` (`NAME`),
  KEY `PHOTO` (`PHOTO`),
  CONSTRAINT `t_court_ibfk_1` FOREIGN KEY (`VENUE_ID`) REFERENCES `t_venue` (`VENUE_ID`),
  CONSTRAINT `t_court_ibfk_2` FOREIGN KEY (`TYPE`) REFERENCES `t_type` (`TYPE_ID`),
  CONSTRAINT `t_court_ibfk_3` FOREIGN KEY (`PHOTO`) REFERENCES `t_gallery_court` (`GALLERY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_court
-- ----------------------------
INSERT INTO `t_court` VALUES ('Court 1', '1', '105000', '1', '1', '3', '10,22');
INSERT INTO `t_court` VALUES ('Court 1 Rumput Sintetis', '1', '130000', '1', '1', '1', '8,20');
INSERT INTO `t_court` VALUES ('Court 2', '1', '105000', '1', '1', '4', null);
INSERT INTO `t_court` VALUES ('Court 2 Rumput Sintetis', '1', '130000', '1', '1', null, '7,22');
INSERT INTO `t_court` VALUES ('Court Badminton', '1', '40000', '2', '3', '23', '7,22');
INSERT INTO `t_court` VALUES ('Court Badminton 2', '1', '50000', '2', '3', '26', '7,22');
INSERT INTO `t_court` VALUES ('Court Gila Keren', '2', '125000', '2', '0', null, null);
INSERT INTO `t_court` VALUES ('Court Indoor', '2', '155000', '1', '1', null, null);
INSERT INTO `t_court` VALUES ('Court Outdoor', '2', '125000', '1', '1', null, null);
INSERT INTO `t_court` VALUES ('Lapang 1', '2', '50000', '2', '3', null, null);
INSERT INTO `t_court` VALUES ('Lapang 2', '2', '50000', '2', '3', null, null);
INSERT INTO `t_court` VALUES ('Lapang 3', '2', '50000', '2', '3', null, null);
INSERT INTO `t_court` VALUES ('Lapang Tenis', '3', '45000', '3', '3', null, null);
INSERT INTO `t_court` VALUES ('Meja 1', '3', '25000', '4', '3', null, null);
INSERT INTO `t_court` VALUES ('Meja 2', '3', '25000', '4', '3', null, null);
INSERT INTO `t_court` VALUES ('Meja 3', '3', '25000', '4', '3', null, null);
INSERT INTO `t_court` VALUES ('Meja 4', '3', '25000', '4', '3', null, null);
INSERT INTO `t_court` VALUES ('Meja Spesial', '3', '40000', '4', '3', null, null);
INSERT INTO `t_court` VALUES ('Tenis Outdoor', '2', '75000', '3', '2', null, null);

-- ----------------------------
-- Table structure for `t_gallery_court`
-- ----------------------------
DROP TABLE IF EXISTS `t_gallery_court`;
CREATE TABLE `t_gallery_court` (
  `GALLERY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VENUE_ID` int(11) NOT NULL,
  `COURT` varchar(255) NOT NULL,
  `PHOTO` varchar(255) NOT NULL,
  PRIMARY KEY (`GALLERY_ID`),
  KEY `COURT` (`COURT`),
  KEY `VENUE_ID` (`VENUE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_gallery_court
-- ----------------------------
INSERT INTO `t_gallery_court` VALUES ('1', '1', 'Court 1', 'IMG_2438.JPG');
INSERT INTO `t_gallery_court` VALUES ('2', '1', 'Court 1', 'IMG_2462.JPG');
INSERT INTO `t_gallery_court` VALUES ('3', '1', 'Court 1', 'IMG_2506.JPG');
INSERT INTO `t_gallery_court` VALUES ('4', '1', 'Court 2', 'IMG_2533.JPG');
INSERT INTO `t_gallery_court` VALUES ('5', '1', 'Court 2', 'IMG_2551.JPG');
INSERT INTO `t_gallery_court` VALUES ('6', '2', 'Court Indoor', 'IMG_2601.JPG');
INSERT INTO `t_gallery_court` VALUES ('7', '2', 'Court Outdoor', 'IMG_2604.JPG');
INSERT INTO `t_gallery_court` VALUES ('12', '-1', 'Court 1', '12_12498823_10205495562048266_54536304_n.jpg');
INSERT INTO `t_gallery_court` VALUES ('13', '-1', 'Court 1', '13_12516380_10205495561648256_1291775522_n.jpg');
INSERT INTO `t_gallery_court` VALUES ('14', '-1', 'Court 1', '14_12494559_10205495740892737_1569845711_o.jpg');
INSERT INTO `t_gallery_court` VALUES ('15', '-1', 'Court 1', '15_11911656_973843429305608_1228646753_n.jpg');
INSERT INTO `t_gallery_court` VALUES ('17', '-1', 'Court 1', '17_11911656_973843429305608_1228646753_n.jpg');
INSERT INTO `t_gallery_court` VALUES ('18', '1', 'Court 1', '18_DEFAULT.jpg');
INSERT INTO `t_gallery_court` VALUES ('19', '1', 'Court 1 Rumput Sintetis', '19_DEFAULT.jpg');
INSERT INTO `t_gallery_court` VALUES ('20', '1', 'Court 1 Rumput Sintetis', '20_IMG_2533.JPG');
INSERT INTO `t_gallery_court` VALUES ('21', '1', 'Court 1 Rumput Sintetis', '21_IMG_2551.JPG');
INSERT INTO `t_gallery_court` VALUES ('22', '1', 'Court Badminton', '22_19_DEFAULT.jpg');
INSERT INTO `t_gallery_court` VALUES ('23', '1', 'Court Badminton', '23_IMG_2604.JPG');
INSERT INTO `t_gallery_court` VALUES ('24', '1', 'Court Badminton 2', '24_19_DEFAULT.jpg');
INSERT INTO `t_gallery_court` VALUES ('25', '1', 'Court Badminton 2', '25_20_IMG_2533.JPG');
INSERT INTO `t_gallery_court` VALUES ('26', '1', 'Court Badminton 2', '26_IMG_2506.JPG');
INSERT INTO `t_gallery_court` VALUES ('27', '1', 'Court Badminton 2', '27_IMG_2533.JPG');

-- ----------------------------
-- Table structure for `t_location`
-- ----------------------------
DROP TABLE IF EXISTS `t_location`;
CREATE TABLE `t_location` (
  `LOCATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COUNTRY` varchar(255) NOT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `MAPS_LAT` double DEFAULT NULL,
  `MAPS_LNG` double DEFAULT NULL,
  PRIMARY KEY (`LOCATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_location
-- ----------------------------
INSERT INTO `t_location` VALUES ('1', 'Indonesia', 'Jakarta', '-6.2293867', '106.689429');
INSERT INTO `t_location` VALUES ('2', 'Indonesia', 'Bandung', '-6.871696', '107.5661903');
INSERT INTO `t_location` VALUES ('3', 'Malaysia', 'Kuala Lumpur', '3.1393364', '101.5467663');
INSERT INTO `t_location` VALUES ('4', 'Thailand', 'Bangkok', '13.7244426', '100.3529157');
INSERT INTO `t_location` VALUES ('5', 'Vietnam', 'Hanoi', '21.0227732', '105.801944');

-- ----------------------------
-- Table structure for `t_schedule`
-- ----------------------------
DROP TABLE IF EXISTS `t_schedule`;
CREATE TABLE `t_schedule` (
  `SCHEDULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VENUE_ID` int(11) NOT NULL,
  `COURT` varchar(255) NOT NULL,
  `TIME` time NOT NULL,
  PRIMARY KEY (`SCHEDULE_ID`),
  KEY `VENUE_ID` (`VENUE_ID`),
  KEY `COURT` (`COURT`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_schedule
-- ----------------------------
INSERT INTO `t_schedule` VALUES ('13', '2', 'Court Indoor', '07:00:00');
INSERT INTO `t_schedule` VALUES ('14', '2', 'Court Indoor', '08:00:00');
INSERT INTO `t_schedule` VALUES ('15', '2', 'Court Indoor', '09:00:00');
INSERT INTO `t_schedule` VALUES ('16', '2', 'Court Indoor', '10:00:00');
INSERT INTO `t_schedule` VALUES ('17', '2', 'Court Indoor', '11:00:00');
INSERT INTO `t_schedule` VALUES ('18', '2', 'Court Indoor', '12:00:00');
INSERT INTO `t_schedule` VALUES ('19', '2', 'Court Outdoor', '07:00:00');
INSERT INTO `t_schedule` VALUES ('20', '2', 'Court Outdoor', '08:00:00');
INSERT INTO `t_schedule` VALUES ('21', '2', 'Court Outdoor', '09:00:00');
INSERT INTO `t_schedule` VALUES ('22', '2', 'Court Outdoor', '10:00:00');
INSERT INTO `t_schedule` VALUES ('23', '2', 'Court Outdoor', '11:00:00');
INSERT INTO `t_schedule` VALUES ('24', '2', 'Court Outdoor', '12:00:00');
INSERT INTO `t_schedule` VALUES ('28', '2', 'Lapang 1', '07:00:00');
INSERT INTO `t_schedule` VALUES ('29', '2', 'Lapang 1', '10:00:00');
INSERT INTO `t_schedule` VALUES ('30', '2', 'Lapang 1', '13:00:00');
INSERT INTO `t_schedule` VALUES ('31', '2', 'Lapang 1', '16:00:00');
INSERT INTO `t_schedule` VALUES ('32', '2', 'Lapang 1', '19:00:00');
INSERT INTO `t_schedule` VALUES ('33', '2', 'Lapang 2', '07:00:00');
INSERT INTO `t_schedule` VALUES ('34', '2', 'Lapang 2', '10:00:00');
INSERT INTO `t_schedule` VALUES ('35', '2', 'Lapang 2', '13:00:00');
INSERT INTO `t_schedule` VALUES ('36', '2', 'Lapang 2', '16:00:00');
INSERT INTO `t_schedule` VALUES ('37', '2', 'Lapang 2', '19:00:00');
INSERT INTO `t_schedule` VALUES ('38', '2', 'Lapang 3', '07:00:00');
INSERT INTO `t_schedule` VALUES ('39', '2', 'Lapang 3', '10:00:00');
INSERT INTO `t_schedule` VALUES ('40', '2', 'Lapang 3', '13:00:00');
INSERT INTO `t_schedule` VALUES ('41', '2', 'Lapang 3', '16:00:00');
INSERT INTO `t_schedule` VALUES ('42', '2', 'Lapang 3', '19:00:00');
INSERT INTO `t_schedule` VALUES ('59', '1', 'Court 1', '10:00:00');
INSERT INTO `t_schedule` VALUES ('60', '1', 'Court 1', '11:00:00');
INSERT INTO `t_schedule` VALUES ('61', '1', 'Court 1', '12:00:00');
INSERT INTO `t_schedule` VALUES ('62', '1', 'Court 1', '13:00:00');
INSERT INTO `t_schedule` VALUES ('63', '1', 'Court 1', '14:00:00');
INSERT INTO `t_schedule` VALUES ('64', '1', 'Court 1', '15:00:00');
INSERT INTO `t_schedule` VALUES ('65', '1', 'Court 1', '16:00:00');
INSERT INTO `t_schedule` VALUES ('66', '1', 'Court 1', '17:00:00');
INSERT INTO `t_schedule` VALUES ('67', '1', 'Court 1', '18:00:00');
INSERT INTO `t_schedule` VALUES ('68', '1', 'Court 1', '19:00:00');
INSERT INTO `t_schedule` VALUES ('69', '1', 'Court 1', '20:00:00');
INSERT INTO `t_schedule` VALUES ('70', '1', 'Court 1', '21:00:00');
INSERT INTO `t_schedule` VALUES ('83', '1', 'Court 1 Rumput Sintetis', '08:00:00');
INSERT INTO `t_schedule` VALUES ('84', '1', 'Court 1 Rumput Sintetis', '09:00:00');
INSERT INTO `t_schedule` VALUES ('85', '1', 'Court 1 Rumput Sintetis', '10:00:00');
INSERT INTO `t_schedule` VALUES ('86', '1', 'Court 1 Rumput Sintetis', '11:00:00');
INSERT INTO `t_schedule` VALUES ('87', '1', 'Court 1 Rumput Sintetis', '12:00:00');
INSERT INTO `t_schedule` VALUES ('88', '1', 'Court 1 Rumput Sintetis', '13:00:00');
INSERT INTO `t_schedule` VALUES ('89', '1', 'Court 1 Rumput Sintetis', '14:00:00');
INSERT INTO `t_schedule` VALUES ('90', '1', 'Court 1 Rumput Sintetis', '15:00:00');
INSERT INTO `t_schedule` VALUES ('91', '1', 'Court 1 Rumput Sintetis', '16:00:00');
INSERT INTO `t_schedule` VALUES ('92', '1', 'Court 1 Rumput Sintetis', '17:00:00');
INSERT INTO `t_schedule` VALUES ('93', '1', 'Court 1 Rumput Sintetis', '18:00:00');
INSERT INTO `t_schedule` VALUES ('94', '1', 'Court 1 Rumput Sintetis', '19:00:00');
INSERT INTO `t_schedule` VALUES ('95', '1', 'Court Badminton', '07:00:00');
INSERT INTO `t_schedule` VALUES ('96', '1', 'Court Badminton', '10:00:00');
INSERT INTO `t_schedule` VALUES ('97', '1', 'Court Badminton', '13:00:00');
INSERT INTO `t_schedule` VALUES ('98', '1', 'Court Badminton', '16:00:00');
INSERT INTO `t_schedule` VALUES ('99', '1', 'Court Badminton', '19:00:00');
INSERT INTO `t_schedule` VALUES ('157', '1', 'Court 2 Rumput Sintetis', '08:00:00');
INSERT INTO `t_schedule` VALUES ('158', '1', 'Court 2 Rumput Sintetis', '09:00:00');
INSERT INTO `t_schedule` VALUES ('159', '1', 'Court 2 Rumput Sintetis', '10:00:00');
INSERT INTO `t_schedule` VALUES ('160', '1', 'Court 2 Rumput Sintetis', '11:00:00');
INSERT INTO `t_schedule` VALUES ('161', '1', 'Court 2 Rumput Sintetis', '12:00:00');
INSERT INTO `t_schedule` VALUES ('162', '1', 'Court 2 Rumput Sintetis', '13:00:00');
INSERT INTO `t_schedule` VALUES ('163', '1', 'Court 2 Rumput Sintetis', '14:00:00');
INSERT INTO `t_schedule` VALUES ('164', '1', 'Court 2 Rumput Sintetis', '15:00:00');
INSERT INTO `t_schedule` VALUES ('165', '1', 'Court 2 Rumput Sintetis', '16:00:00');
INSERT INTO `t_schedule` VALUES ('166', '1', 'Court 2 Rumput Sintetis', '17:00:00');
INSERT INTO `t_schedule` VALUES ('167', '1', 'Court 2 Rumput Sintetis', '18:00:00');
INSERT INTO `t_schedule` VALUES ('168', '1', 'Court 2 Rumput Sintetis', '19:00:00');
INSERT INTO `t_schedule` VALUES ('169', '1', 'Court 2 Rumput Sintetis', '20:00:00');
INSERT INTO `t_schedule` VALUES ('170', '1', 'Court 2 Rumput Sintetis', '21:00:00');
INSERT INTO `t_schedule` VALUES ('171', '1', 'Court 2 Rumput Sintetis', '07:00:00');
INSERT INTO `t_schedule` VALUES ('172', '1', 'Court Badminton 2', '07:00:00');
INSERT INTO `t_schedule` VALUES ('173', '1', 'Court Badminton 2', '10:00:00');
INSERT INTO `t_schedule` VALUES ('174', '1', 'Court Badminton 2', '13:00:00');
INSERT INTO `t_schedule` VALUES ('175', '1', 'Court Badminton 2', '16:00:00');
INSERT INTO `t_schedule` VALUES ('176', '1', 'Court Badminton 2', '19:00:00');
INSERT INTO `t_schedule` VALUES ('177', '1', 'Court 2', '10:00:00');

-- ----------------------------
-- Table structure for `t_type`
-- ----------------------------
DROP TABLE IF EXISTS `t_type`;
CREATE TABLE `t_type` (
  `TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(255) NOT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_type
-- ----------------------------
INSERT INTO `t_type` VALUES ('1', 'Futsal');
INSERT INTO `t_type` VALUES ('2', 'Badminton');
INSERT INTO `t_type` VALUES ('3', 'Tenis');
INSERT INTO `t_type` VALUES ('4', 'Ping Pong');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,
  `USERNAME` varchar(255) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `PHONE` char(255) DEFAULT NULL,
  `IDENTITY` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'Demo', 'User', 'demouser', 'demouser@playhouze.com', '088130911825', '10100928810', 'demouser');
INSERT INTO `t_user` VALUES ('2', 'Aldi', 'Zainafif', 'zainafifaldi', 'aldi.zainafif@gmail.com', '085720029976', '1823019260', 'aldizain');
INSERT INTO `t_user` VALUES ('3', 'Aldi', 'Zain', 'aldizain', 'zain@aldi', '103012378', '12381092380', 'aldi');

-- ----------------------------
-- Table structure for `t_venue`
-- ----------------------------
DROP TABLE IF EXISTS `t_venue`;
CREATE TABLE `t_venue` (
  `VENUE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `ADDRESS` text NOT NULL,
  `MAPS_LAT` double DEFAULT NULL,
  `MAPS_LNG` double DEFAULT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `DESCRIPTION` text,
  `PUBLISH` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`VENUE_ID`),
  KEY `LOCATION_ID` (`LOCATION_ID`),
  CONSTRAINT `t_venue_ibfk_1` FOREIGN KEY (`LOCATION_ID`) REFERENCES `t_location` (`LOCATION_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_venue
-- ----------------------------
INSERT INTO `t_venue` VALUES ('1', 'Sampoerna', 'Jl. Setiabudhi Atas', '-6.8980849', '107.638463', '2', '<p></p><p></p><ol><li>Sampoerna adalah lapangan futsal terbaik di <u><b>Setiabudhi</b></u>.<br></li><li>Sampoerna memiliki lapangan indoor dan outdoor.<br></li><li>Pada lapangan indoor terdapat lapangan dengan rumput sintetis.<br></li><li><a target=\"_blank\" rel=\"nofollow\" href=\"http://playhouseapp.com\">http://playhouseapp.com/</a> <br></li></ol><p></p><p></p>', '\0');
INSERT INTO `t_venue` VALUES ('2', 'Kebon Manggi Sport', 'Jl. Kebon Manggi Jakarta', '-6.2054856', '106.8546941', '1', '<p>Kebon Manggi Sport adalah venue terlengkap di seluruh Jakarta. Nikmati fasilitas yang tak terbatas.</p>', '\0');
INSERT INTO `t_venue` VALUES ('3', 'Lebak Bulus Sport', 'Jl. Lebak Bulus Jakarta', null, null, '1', '<p>asd</p>', '\0');
INSERT INTO `t_venue` VALUES ('4', 'Kate Minangka', 'Pr. 75 Malaysia Timur, Kuala Lumpur', null, null, '3', '<p>asd</p>', '\0');
INSERT INTO `t_venue` VALUES ('5', 'Kate Pirang Mahua', 'Pr. 88 Malaysia Selatan Dekat Singapore', null, null, '3', '<p>asd</p>', '\0');
