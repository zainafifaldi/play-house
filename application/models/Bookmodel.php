<?php
class Bookmodel extends CI_Model {
	
	private $eGHL_charge = array("DD" => 1.5, "CC" => 2.8, "PLUS" => 6);
	private $playHouse_charge = array("DD" => 1.41, "CC" => 1.5);
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		
		date_default_timezone_set("Asia/Jakarta");
		$this->cancel_deprecated_book();
	}
	
	function get_charge()
	{
		return array("eGHL" => $this->eGHL_charge, "playHouse" => $this->playHouse_charge);
	}
	
	function cancel_deprecated_book()
	{
		if(date("i") >= 30) {
			$dateHour = date("Y-m-d H");
			$minutes = date("i") - 30;
			
			$timestamp = $dateHour . ":" . $minutes . ":00";
			
			$this->db->query("UPDATE t_book SET LOCK_STATE = ? WHERE LOCK_STATE = ? AND BOOK_TIMESTAMP < ?", array(0, 1, $timestamp));
		}
	}
	
	function get_order_list_by_user($userId)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_book.*, t_schedule.*, t_venue.*, t_court.TYPE, t_type.TYPE AS TYPE_NAME, t_payment.eGHL_CHARGE, t_payment.PH_CHARGE
																FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_venue ON t_venue.VENUE_ID = t_schedule.VENUE_ID
																JOIN t_court ON t_court.VENUE_ID = t_schedule.VENUE_ID AND
																								t_court.NAME = t_schedule.COURT
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																LEFT JOIN t_payment ON t_payment.RESERVATION = t_book.RESERVATION
																WHERE t_book.USER_ID = ?
																GROUP BY t_book.RESERVATION
																ORDER BY t_book.BOOK_TIMESTAMP DESC", array($userId));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_schedule_by_reservation($reservation)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_schedule
																JOIN t_book ON t_book.SCHEDULE_ID = t_schedule.SCHEDULE_ID
																WHERE t_book.RESERVATION = ?
																ORDER BY t_schedule.TIME ASC", array($reservation));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_all_order($limit = 1000000)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *, t_type.TYPE AS TYPE_NAME, SUM(t_book.PRICE) AS BOOK_PRICE, t_venue.VENUE_ID AS VENUE_ID, t_venue.NAME AS NAME FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_court ON t_court.VENUE_ID = t_schedule.VENUE_ID AND
																								t_court.NAME = t_schedule.COURT
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																JOIN t_venue ON t_venue.VENUE_ID = t_court.VENUE_ID
																WHERE t_book.LOCK_STATE != ?
																GROUP BY t_book.RESERVATION
																ORDER BY t_book.BOOK_TIMESTAMP DESC
																LIMIT ?", array(0, $limit));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_weekly_order($minusFrom, $minusTo)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *,
																			 t_venue.VENUE_ID AS VENUE_ID,
																			 COUNT(t_payment.eGHL_CHARGE) AS TOTAL_BOOK,
																			 SUM(t_book.PRICE) AS TOTAL_PRICE,
																			 SUM(t_payment.eGHL_CHARGE) AS TOTAL_eGHL,
																			 SUM(t_payment.PH_CHARGE) AS TOTAL_PH
																FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_venue ON t_venue.VENUE_ID = t_schedule.VENUE_ID
																LEFT JOIN t_payment ON t_payment.RESERVATION = t_book.RESERVATION
																WHERE t_book.LOCK_STATE = ? AND
																			t_book.BOOK_TIMESTAMP >= (NOW() - INTERVAL ? DAY) AND
																			t_book.BOOK_TIMESTAMP <= (NOW() - INTERVAL ? DAY)
																GROUP BY t_schedule.VENUE_ID", array(2, $minusFrom, $minusTo));
																// print_r($this->db->last_query());
																// exit;
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_weekly_order_detail($venue, $minusFrom, $minusTo)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *,
																			 t_venue.VENUE_ID AS VENUE_ID,
																			 SUM(t_book.PRICE) AS BOOK_PRICE,
																			 t_type.TYPE AS TYPE_NAME,
																			 t_book.RESERVATION AS RESERVATION,
																			 t_venue.NAME AS NAME
																FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_court ON t_court.VENUE_ID = t_schedule.VENUE_ID AND
																								t_court.NAME = t_schedule.COURT
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																JOIN t_venue ON t_venue.VENUE_ID = t_schedule.VENUE_ID
																LEFT JOIN t_payment ON t_payment.RESERVATION = t_book.RESERVATION
																WHERE t_book.LOCK_STATE = ? AND
																			t_book.BOOK_TIMESTAMP >= (NOW() - INTERVAL ? DAY) AND
																			t_book.BOOK_TIMESTAMP <= (NOW() - INTERVAL ? DAY) AND
																			t_venue.VENUE_ID = ?
																GROUP BY t_book.RESERVATION
																ORDER BY t_book.BOOK_TIMESTAMP", array(2, $minusFrom, $minusTo, $venue));
																// print_r($this->db->last_query());
																// exit;
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_plan_order_by_venue($venueId, $success)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *, t_type.TYPE AS TYPE_NAME, SUM(t_book.PRICE) AS BOOK_PRICE FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_court ON t_court.VENUE_ID = t_schedule.VENUE_ID AND
																								t_court.NAME = t_schedule.COURT
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																WHERE t_book.LOCK_STATE = ? AND
																			t_schedule.VENUE_ID = ? AND
																			t_book.BOOK_DATE >= ?
																GROUP BY t_book.RESERVATION
																ORDER BY t_book.BOOK_TIMESTAMP DESC", array($success, $venueId, date("Y-m-d")));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_history_order_by_venue($venueId)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *, t_type.TYPE AS TYPE_NAME, SUM(t_book.PRICE) AS BOOK_PRICE FROM t_book
																JOIN t_schedule ON t_schedule.SCHEDULE_ID = t_book.SCHEDULE_ID
																JOIN t_court ON t_court.VENUE_ID = t_schedule.VENUE_ID AND
																								t_court.NAME = t_schedule.COURT
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																WHERE t_book.LOCK_STATE = ? AND
																			t_schedule.VENUE_ID = ? AND
																			t_book.BOOK_DATE < ?
																GROUP BY t_book.RESERVATION
																ORDER BY t_book.BOOK_TIMESTAMP DESC", array(2, $venueId, date("Y-m-d")));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_last_reservation_id()
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT RESERVATION FROM t_book ORDER BY RESERVATION DESC LIMIT 1");
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function search_one($table, $data)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->get_where($table, $data);
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function search_more($table, $data)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->get_where($table, $data);
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function insert($table, $data)
	{
		$isError = false;
		$result = array();
		
		$this->db->insert($table, $data);
		
		if($this->db->affected_rows() > 0) {
			$result = $this->db->insert_id();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function update($table, $data, $condition)
	{
		$isError = false;
		$result = array();
		
		$this->db->where($condition);
		$this->db->update($table, $data);
		
		if($this->db->affected_rows() <= 0) {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function delete($table, $condition)
	{
		$isError = false;
		$result = array();
		
		$this->db->where($condition);
		$this->db->delete($table);
		
		if($this->db->affected_rows() <= 0) {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
}