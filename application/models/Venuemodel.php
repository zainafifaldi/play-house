<?php
class Venuemodel extends CI_Model {
	
	private $queryStrict;
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	function get_last_court()
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT *, t_type.TYPE AS TYPE_NAME, t_court.NAME AS COURT_NAME FROM t_court
																JOIN t_type ON t_type.TYPE_ID = t_court.TYPE
																JOIN t_venue ON t_venue.VENUE_ID = t_court.VENUE_ID
																JOIN t_location ON t_location.LOCATION_ID = t_venue.LOCATION_ID
																LEFT JOIN t_gallery_court ON t_gallery_court.GALLERY_ID = t_court.PHOTO
																ORDER BY t_court.TIMESTAMP DESC
																LIMIT 10");
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_all_type()
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_type");
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_all_venue()
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_venue
																JOIN t_location ON t_location.LOCATION_ID = t_venue.LOCATION_ID");
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_venue_by_loc_type($location, $type)
	{
		$isError = false;
		$result = array();
		
		$where = "";
		$parameters = array();
		
		if($location) {
			if($where != "")
				$where .= " AND";
			$where .= " t_location.LOCATION_ID = ?";
			$parameters[] = $location;
		}
		if($type) {
			if($where != "")
				$where .= " AND";
			$where .= " t_type.TYPE_ID = ?";
			$parameters[] = $type;
		}
		
		$where = $where ? " WHERE" . $where : "";
		
		$query = "SELECT t_venue.*
							FROM t_venue
							LEFT JOIN t_location ON
												t_location.LOCATION_ID = t_venue.LOCATION_ID
							JOIN t_court ON
									 t_court.VENUE_ID = t_venue.VENUE_ID
							LEFT JOIN t_type ON
												t_type.TYPE_ID = t_court.TYPE" .
							$where ."
							GROUP BY t_venue.NAME";
		$search = $this->db->query($query, $parameters);
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_venue_with_search_strict($location, $type, $venue, $date, $start, $end, $group = TRUE)
	{
		$isError = false;
		$result = array();
		
		$query = "SELECT t_venue.*,
										 t_court.NAME AS COURT_NAME,
										 t_court.TYPE AS TYPE_ID,
										 t_gallery_court.PHOTO,
										 t_type.TYPE AS TYPE_NAME,
										 t_location.COUNTRY,
										 t_location.CITY,";
		if($group) {
			$query .= "		 MIN(t_court.PRICE) AS PRICE_FROM, 
										 MAX(t_court.PRICE) AS PRICE_TO
							FROM t_venue";
		}
		else {
			$query .= "		 t_court.PRICE AS PRICE
							FROM t_venue";
		}
		
		$join = " JOIN t_court ON t_court.VENUE_ID = t_venue.VENUE_ID";
		$join .= " JOIN t_location ON t_location.LOCATION_ID = t_venue.LOCATION_ID";
		$join .= " JOIN t_type ON t_type.TYPE_ID = t_court.TYPE";
		$join .= " LEFT JOIN t_gallery_court ON t_gallery_court.GALLERY_ID = t_court.PHOTO";
		
		$where = "";
		$parameters = array();
		
		if($venue) {
			if($where != "")
				$where .= " AND";
			$where .= " t_venue.VENUE_ID = ?";
			$parameters[] = $venue;
		}
		else if($location) {
			if($where != "")
				$where .= " AND";
			$where .= " t_location.LOCATION_ID = ?";
			$parameters[] = $location;
		}
		
		if($type) {
			if($where != "")
				$where .= " AND";
			$where .= " t_type.TYPE_ID = ?";
			$parameters[] = $type;
		}
		
		if($date || $start) {
				if($where != "")
					$where .= " AND";
				
				$where .= " (t_court.VENUE_ID, t_court.NAME) IN
												 (SELECT t_schedule.VENUE_ID, t_schedule.COURT
													FROM t_schedule
													WHERE ";
			if($date) {
				$day = date("D", strtotime($date));
				
				$where .= "							t_schedule.SCHEDULE_ID NOT IN (SELECT t_book.SCHEDULE_ID FROM t_book WHERE t_book.BOOK_DATE = '$date' AND t_book.LOCK_STATE != 0) AND";
				$where .= "							t_schedule.DAY = '$day'";
			}
			if($start && $date)
				$where .= "							AND";
			if($start) {
				$where .= "							t_schedule.TIME >= '$start:00' AND t_schedule.TIME < '$end:00'";
			}
			$where .= "							)";
		}
		
		$where = $where ? " WHERE" . $where : "";
		
		$this->queryStrict = "SELECT t_court.VENUE_ID, t_court.TYPE FROM t_venue" . $join . $where . " GROUP BY t_court.VENUE_ID, t_court.TYPE";
		
		$groupBy = "";
		if($group)
			$groupBy = " GROUP BY t_court.VENUE_ID, t_court.TYPE";
		
		$query = $query . $join . $where . $groupBy;
		// echo "<pre>";
		// echo $query;
		// exit;
		$search = $this->db->query($query, $parameters);
		// print_r($search);
		// exit;
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_venue_with_search_tolerant($location, $type, $venue, $date, $start, $end)
	{
		$isError = false;
		$result = array();
		
		$query = "SELECT t_venue.*,
										 t_court.NAME AS COURT_NAME,
										 t_court.TYPE AS TYPE_ID,
										 t_gallery_court.PHOTO,
										 t_type.TYPE_ID AS TYPE,
										 t_type.TYPE AS TYPE_NAME,
										 t_location.COUNTRY,
										 t_location.CITY,
										 MIN(t_court.PRICE) AS PRICE_FROM, 
										 MAX(t_court.PRICE) AS PRICE_TO 
							FROM t_venue";
		
		$join = " JOIN t_court ON t_court.VENUE_ID = t_venue.VENUE_ID";
		$join .= " JOIN t_location ON t_location.LOCATION_ID = t_venue.LOCATION_ID";
		$join .= " JOIN t_type ON t_type.TYPE_ID = t_court.TYPE";
		$join .= " LEFT JOIN t_gallery_court ON t_gallery_court.GALLERY_ID = t_court.PHOTO";
		
		$where = "";
		$parameters = array();
		
		if($venue) {
			if($where != "")
				$where .= " AND";
			$where .= " t_venue.VENUE_ID = ?";
			$parameters[] = $venue;
		}
		else if($location) {
			if($where != "")
				$where .= " AND";
			$where .= " t_location.LOCATION_ID = ?";
			$parameters[] = $location;
		}
		
		if($type) {
			if($where != "")
				$where .= " AND";
			$where .= " t_type.TYPE_ID = ?";
			$parameters[] = $type;
		}
		
		if($date || $start) {
				if($where != "")
					$where .= " AND";
				
				$where .= " (t_court.VENUE_ID, t_court.NAME) IN
												 (SELECT t_schedule.VENUE_ID, t_schedule.COURT
													FROM t_schedule
													WHERE ";
			if($start)
				$where .= "							t_schedule.TIME >= '$start:00' AND t_schedule.TIME < '$end:00'";
			if($start && $date)
				$where .= "							AND";
			if($date) {
				$day = date("D", strtotime($date));
				
				$where .= "							t_schedule.SCHEDULE_ID NOT IN (SELECT SCHEDULE_ID FROM t_book WHERE BOOK_DATE='$date' AND t_book.LOCK_STATE != 0) AND";
				$where .= "							t_schedule.DAY = '$day'";
			}
			$where .= "							)";
		}
		
		if($this->queryStrict) {
			if($where != "")
				$where .= " AND";
			$where .= " (t_court.VENUE_ID, t_court.TYPE) NOT IN (". $this->queryStrict .")";
		}
		
		$where = $where ? " WHERE" . $where : "";
		
		$query = $query . $join . $where . " GROUP BY t_court.VENUE_ID, t_court.TYPE";
		// echo $query;
		// exit;
		$search = $this->db->query($query, array_merge($parameters, $parameters));
		// print_r($search);
		// exit;
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_available_venue_by_date($type, $venue, $date)
	{
		$isError = false;
		$result = array();
		
		$day = date("D", strtotime($date));
		
		$query = "SELECT t_court.NAME AS COURT_NAME,
										 t_court.PRICE AS PRICE,
										 t_schedule.TIME AS TIME,
										 t_schedule.SCHEDULE_ID AS ID
							FROM t_court
							JOIN t_schedule ON t_schedule.VENUE_ID = t_court.VENUE_ID
															AND t_schedule.COURT = t_court.NAME
							WHERE t_court.VENUE_ID = ? AND
										t_court.TYPE = ? AND
										t_schedule.DAY = ? AND
										t_schedule.SCHEDULE_ID NOT IN
										(SELECT SCHEDULE_ID FROM t_book WHERE BOOK_DATE = ? AND t_book.LOCK_STATE != 0)";
		
		$search = $this->db->query($query, array($venue, $type, $day, $date));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_available_venue_by_schedule_id($scheduleId, $date)
	{
		$isError = false;
		$result = array();
		
		$day = date("D", strtotime($date));
		
		$query = "SELECT t_court.NAME AS COURT_NAME,
										 t_court.PRICE AS PRICE,
										 t_schedule.TIME AS TIME,
										 t_schedule.SCHEDULE_ID AS ID
							FROM t_court
							JOIN t_schedule ON t_schedule.VENUE_ID = t_court.VENUE_ID
															AND t_schedule.COURT = t_court.NAME
							WHERE (t_court.VENUE_ID, t_court.NAME) IN
										(SELECT VENUE_ID, COURT FROM t_schedule WHERE SCHEDULE_ID = ?)
										AND
										t_schedule.DAY = ? AND
										t_schedule.SCHEDULE_ID NOT IN
										(SELECT SCHEDULE_ID FROM t_book WHERE BOOK_DATE = ? AND t_book.LOCK_STATE != 0)";
		
		$search = $this->db->query($query, array($scheduleId, $day, $date));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_venue_detail($ven)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_venue.*,
																			 t_location.COUNTRY,
																			 t_location.CITY,
																			 t_location.MAPS_LAT AS MAPS_LAT_LOC,
																			 t_location.MAPS_LNG AS MAPS_LNG_LOC
																FROM t_venue
																JOIN t_location ON t_location.LOCATION_ID = t_venue.LOCATION_ID
																WHERE t_venue.VENUE_ID = ?", array($ven));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_list_court_by_venue($ven)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_court.*,
																			 t_gallery_court.PHOTO AS PHOTO
																FROM t_court
																LEFT JOIN t_gallery_court ON
																					t_gallery_court.GALLERY_ID = t_court.PHOTO
																WHERE t_court.VENUE_ID = ?", array($ven));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_list_court_by_venue_type($ven, $tp)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_court.*,
																			 t_gallery_court.PHOTO AS PHOTO
																FROM t_court
																LEFT JOIN t_gallery_court ON
																					t_gallery_court.GALLERY_ID = t_court.PHOTO
																WHERE t_court.VENUE_ID = ? AND
																			t_court.TYPE = ?", array($ven, $tp));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_gallery_by_venue_type($ven, $tp)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_gallery_court.*
																FROM t_gallery_court
																WHERE t_gallery_court.VENUE_ID = ? AND
																			t_gallery_court.COURT IN
																			(SELECT NAME
																			 FROM t_court
																			 WHERE VENUE_ID = ? AND
																						 TYPE = ?)
																			AND
																			t_gallery_court.GALLERY_ID NOT IN
																			(SELECT CASE
																							WHEN PHOTO IS NULL
																							THEN 0
																							ELSE PHOTO END
																			 FROM t_court
																			 WHERE VENUE_ID = ? AND
																						 TYPE = ?)", array($ven, $ven, $tp, $ven, $tp));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_gallery_by_venue_type2($ven, $tp)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_gallery_court.*
																FROM t_gallery_court
																WHERE t_gallery_court.VENUE_ID = ? AND
																			t_gallery_court.COURT IN
																			(SELECT NAME
																			 FROM t_court
																			 WHERE VENUE_ID = ? AND
																						 TYPE = ?)", array($ven, $ven, $tp));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_court_detail($ven, $name)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_court.*,
																			 t_gallery_court.PHOTO AS PHOTO
																FROM t_court
																LEFT JOIN t_gallery_court ON t_gallery_court.GALLERY_ID = t_court.PHOTO
																WHERE t_court.VENUE_ID = ? AND
																			t_court.NAME = ?", array($ven, $name));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_gallery_by_court($ven, $name)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT t_gallery_court.*
																FROM t_gallery_court
																WHERE t_gallery_court.VENUE_ID = ? AND
																			t_gallery_court.COURT = ? AND
																			t_gallery_court.GALLERY_ID NOT IN
																			(SELECT CASE
																							WHEN PHOTO IS NULL
																							THEN 0
																							ELSE PHOTO END
																			 FROM t_court
																			 WHERE VENUE_ID = ? AND
																						 NAME = ?)", array($ven, $name, $ven, $name));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_schedule_detail($venue, $court, $time, $day)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_schedule WHERE VENUE_ID = ? AND COURT = ? AND TIME >= ? AND DAY = ? ORDER BY TIME LIMIT 1", array($venue, $court, $time, $day));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_court_schedule_list($venue, $court)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_schedule WHERE VENUE_ID = ? AND COURT = ?", array($venue, $court));
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_court_detail_by_schedule_id($scheduleId)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_court WHERE (VENUE_ID, NAME) IN (SELECT VENUE_ID, COURT FROM t_schedule WHERE SCHEDULE_ID = ?)", array($scheduleId));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function get_type_detail($type)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_type WHERE TYPE_ID = ?", array($type));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function insert($table, $data)
	{
		$isError = false;
		$result = array();
		
		$this->db->insert($table, $data);
		
		if($this->db->affected_rows() > 0) {
			$result = $this->db->insert_id();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function update($table, $data, $condition)
	{
		$isError = false;
		$result = array();
		
		$this->db->where($condition);
		$this->db->update($table, $data);
		
		if($this->db->affected_rows() <= 0) {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function delete($table, $condition)
	{
		$isError = false;
		$result = array();
		
		$this->db->where($condition);
		$this->db->delete($table);
		
		if($this->db->affected_rows() <= 0) {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
}