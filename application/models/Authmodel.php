<?php
class Authmodel extends CI_Model {
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	function admin_sign_in_check($userid, $password)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_admin WHERE USERNAME = ? AND PASSWORD = ? AND AUTH = ?", array($userid, $password, "adm"));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function vendor_sign_in_check($userid, $password)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_admin WHERE USERNAME = ? AND PASSWORD = ? AND AUTH = ?", array($userid, $password, "ven"));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function user_sign_in_check($username, $password)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_user WHERE USERNAME = ? AND PASSWORD = ?", array($username, $password));
		
		if($search->num_rows() > 0) {
			$result = $search->row();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
	function check_username($username)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT USER_ID FROM t_user WHERE USERNAME = ?", array($username));
		
		if($search->num_rows() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function check_admin_username($username)
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT ADMIN_ID FROM t_admin WHERE USERNAME = ?", array($username));
		
		if($search->num_rows() > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	function insert($table, $data)
	{
		$isError = false;
		$result = array();
		
		$this->db->insert($table, $data);
		
		if($this->db->affected_rows() > 0) {
			$result = $this->db->insert_id();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
}
?>