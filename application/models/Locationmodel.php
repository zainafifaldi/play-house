<?php
class Locationmodel extends CI_Model {
	
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}
	
	function get_all_db_location()
	{
		$isError = false;
		$result = array();
		
		$search = $this->db->query("SELECT * FROM t_location ORDER BY COUNTRY");
		
		if($search->num_rows() > 0) {
			$result = $search->result();
		}
		else {
			$isError = true;
		}
		
		$response = array(
						"is_error" => $isError,
						"result" => $result
					);
		
		return $response;
	}
	
}