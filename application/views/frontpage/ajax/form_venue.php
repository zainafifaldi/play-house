<option value="">- Choose Venue -</option>
<?php
	if(!$venue['is_error']) {
		foreach($venue['result'] as $row) {
			echo "<option value=\"{$row->VENUE_ID}\"";
			if(isset($ven))
				if($ven == $row->VENUE_ID)
					echo " selected=\"selected\"";
			echo ">{$row->NAME}</option>";
		}
	}
	else {
		echo "<optgroup label=\"No Venue Available\"></optgroup>";
	}
?>