<option value="">- Choose Location -</option>
<?php
	if(isset($new))
		echo "<option value=\"new\">New Location</option>";
	
	if(!$location['is_error']) {
		$countryNow = "";
		$i = 0;
		foreach($location['result'] as $row) {
			if($row->COUNTRY != $countryNow) {
				if($i != 0)
					echo "</optgroup>";
				
				$countryNow = $row->COUNTRY;
				echo "<optgroup label=\"{$row->COUNTRY}\">";
			}
			
			echo "<option value=\"{$row->LOCATION_ID}\"";
			if(isset($loc))
				if($loc == $row->LOCATION_ID)
					echo " selected=\"selected\"";
			echo ">{$row->CITY}</option>";
			$i++;
		}
	
		if($i != 0)
			echo "</optgroup>";
	}
	else {
		echo "<optgroup label=\"No Location Available\"></optgroup>";
	}
?>