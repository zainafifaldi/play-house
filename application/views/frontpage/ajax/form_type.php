<option value="">- Choose Type -</option>
<?php
	if(!$type['is_error']) {
		foreach($type['result'] as $row) {
			echo "<option value=\"{$row->TYPE_ID}\"";
			if(isset($tp))
				if($tp == $row->TYPE_ID)
					echo " selected=\"selected\"";
			echo ">{$row->TYPE}</option>";
		}
	}
	else {
		echo "<optgroup label=\"No Type Available\"></optgroup>";
	}
?>