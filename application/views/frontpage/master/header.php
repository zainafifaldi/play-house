<header id="header"><!--header-->
	<div class="header_top"><!--header_top-->
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-push-6">
					<div class="social-icons pull-right">
						<ul class="nav navbar-nav">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!--/header_top-->
	
	<div class="header-middle"><!--header-middle-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="logo pull-left">
						<a href="<?=site_url()?>"><img src="<?=base_url("assets/images/logo/playhouse_logo.png")?>" alt="" /></a>
					</div>
				</div>
				<div class="col-sm-8">
					<div class="shop-menu pull-right">
						<ul class="nav navbar-nav">
							<?php 
								if($this->session->userdata("log_user")) {
									if($this->session->userdata("auth") == "user") {
										echo '<li><a href="#">Welcome, '. $this->session->userdata("first_name") .'!</a></li>';
										echo '<li><a href="'. site_url("page/order_list") .'"><i class="fa fa-flag"></i> Your Order</a></li>';
										echo '<li><a href="'. site_url("page/signout") .'"><i class="fa fa-power-off"></i> Logout</a></li>';
									}
									else {
										echo '<li><a href="'. site_url("page/signin") .'"><i class="fa fa-lock"></i> Login</a></li>';
										echo '<li><a href="'. site_url("vendor") .'"><i class="fa fa-briefcase"></i> Vendor</a></li>';
									}
								}
								else {
									echo '<li><a href="'. site_url("page/signin") .'"><i class="fa fa-lock"></i> Login</a></li>';
									echo '<li><a href="'. site_url("vendor") .'"><i class="fa fa-briefcase"></i> Vendor</a></li>';
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div><!--/header-middle-->
	
</header><!--/header-->