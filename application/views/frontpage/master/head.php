<!DOCTYPE html>
<script>
	if(screen.width <= 820) {
		<?php $backURI = explode("page", $_SERVER['REQUEST_URI']);?>
		window.location = "<?=site_url("page/m")?><?=isset($backURI[1]) ? $backURI[1] : ""?>";
	}
</script>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?=$title?></title>
    <link type="text/css" href="<?=base_url("assets/css/bootstrap.css")?>" rel="stylesheet"/>
		<link type="text/css" href="<?=base_url("assets/css/select2.css")?>" rel="stylesheet"/>
		<link type="text/css" href="<?=base_url("assets/css/bootstrap-datetimepicker.css")?>" rel="stylesheet"/>
    <link type="text/css" href="<?=base_url("assets/css/font-awesome.css")?>" rel="stylesheet">
    <link type="text/css" href="<?=base_url("assets/css/prettyPhoto.css")?>" rel="stylesheet">
    <link type="text/css" href="<?=base_url("assets/css/price-range.css")?>" rel="stylesheet">
    <link type="text/css" href="<?=base_url("assets/css/animate.css")?>" rel="stylesheet">
		<link type="text/css" href="<?=base_url("assets/css/main.css")?>" rel="stylesheet">
		<link type="text/css" href="<?=base_url("assets/css/responsive.css")?>" rel="stylesheet">
		<link type="text/css" href="<?=base_url("assets/css/custom.css")?>" rel="stylesheet">
		<link type="text/css" href="<?=base_url("assets/css/terms_conditions.css")?>" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?=base_url("assets/images/ico/favicon.ico")?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=base_url("assets/images/ico/apple-touch-icon-144-precomposed.png")?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=base_url("assets/images/ico/apple-touch-icon-114-precomposed.png")?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=base_url("assets/images/ico/apple-touch-icon-72-precomposed.png")?>">
    <link rel="apple-touch-icon-precomposed" href="<?=base_url("assets/images/ico/apple-touch-icon-57-precomposed.png")?>">
</head><!--/head-->
<body>
	<script src="<?=base_url("assets/js/jquery-2.2.0.min.js")?>"></script>