<!DOCTYPE html> 
<html>
<head>
	<title><?=$title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<link rel="stylesheet" href="<?=base_url("assets/jquery.mobile.squareui/jquery.mobile.squareui.css")?>" />
	<link type="text/css" href="<?=base_url("assets/css/terms_conditions.css")?>" rel="stylesheet">
	<link rel="stylesheet" href="<?=base_url("assets/css/swiper.min.css")?>">
	<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<script src="<?=base_url("assets/js/swiper.min.js")?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<style>
		a {
			font-weight: normal!important;
			text-decoration: none;
		}
		.seoquake-nofollow {
			text-decoration: none!important;
		}
	</style>
</head>

<body>
	<script>
		$( document ).on( "click", ".show-page-loading-msg", function() {
			var $this = $( this ),
									theme = $this.jqmData( "theme" ) || $.mobile.loader.prototype.options.theme,
									msgText = $this.jqmData( "msgtext" ) || $.mobile.loader.prototype.options.text,
									textVisible = $this.jqmData( "textvisible" ) || $.mobile.loader.prototype.options.textVisible,
									textonly = !!$this.jqmData( "textonly" );
									html = $this.jqmData( "html" ) || "";
			$.mobile.loading( "show", {
													text: msgText,
													textVisible: textVisible,
													theme: theme,
													textonly: textonly,
													html: html
											});
		})
		.on( "click", "form", function() {
			$.mobile.loading( "hide" );
		});
		$(document).ready(function(){
			$(".show-page-loading-msg").click();
		});
	</script>
	<div data-role="page" data-theme="b">
		<div data-role="header">
				<a href="#myMenu" data-icon="grid">&nbsp;</a>
				<h1>PlayHouse</h1>
			</div>
			<div data-role="panel" id="myMenu" data-position="left">
				<?php 
					if($this->session->userdata("log_user")) {
						if($this->session->userdata("auth") == "user") {
							echo '<a href="#">Welcome, '. $this->session->userdata("first_name") .'!</a>';
							echo '<a href="'. site_url("page/order_list") .'" data-role="button" data-icon="bullets" data-ajax="false">Your Order</a>';
							echo '<a href="'. site_url("page/signout") .'" data-role="button" data-icon="power" data-ajax="false">Logout</a>';
						}
						else {
							echo '<a href="'. site_url("page/signin") .'" data-role="button" data-icon="lock" data-ajax="false">Login</a>';
						}
					}
					else {
						echo '<a href="'. site_url("page/signin") .'" data-role="button" data-icon="lock" data-ajax="false">Login</a>';
					}
				?>
				
				<hr/>
				
				<form action="<?=site_url("page/m/search_result")?>" method="GET" class="form-horizontal">
					<label for="form-search-location" class="select">Location</label>
					<select name="loc" id="form-search-location-menu" data-mini="true">
						<?php
							$this->load->view("frontpage/ajax/form_location", array("location" => $location))
						?>
					</select>
					
					<label for="form-search-type" class="select">Type</label>
					<select name="tp" id="form-search-type-menu" data-mini="true">
						<?php
							$this->load->view("frontpage/ajax/form_type", array("type" => $type))
						?>
					</select>
					
					<label for="form-search-venue" class="select">Venue</label>
					<select name="ven" id="form-search-venue-menu" data-mini="true">
						<?php
							$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue))
						?>
					</select>
					<script>
						$("#form-search-location-menu, #form-search-type-menu").change(function () {
							$.ajax({
								type:'GET',
								url:'<?=site_url('page/form_get_venue')?>',
								data:{
											'location':$("#form-search-location-menu").val(), 
											'type':$("#form-search-type-menu").val()
										 },
								success:function(data){
									$("#form-search-venue-menu").html(data);
								},
								error:function(data){
									$("#form-search-venue-menu").html("<optgroup label=\"Error!\"></optgroup>");
								}
							});
						});
					</script>
					
					<label for="form-search-date" class="">Book Date</label>
					<input type="date" name="d" id="form-search-date-menu" value="" />
					
					<div data-role="rangeslider" data-mini="true">
						<label for="range-2a" class="">Available In</label>
						<input type="range" name="rt-from" id="range-2a-menu" min="0" max="24" value="13">
						<label for="range-2b" class="">Available In</label>
						<input type="range" name="rt-to" id="range-2b-menu" min="0" max="24" value="15">
					</div>
					
					<button type="submit" class="ui-btn ui-mini ui-icon-search ui-btn-icon-left">Find It</button>
				</form>
			</div>