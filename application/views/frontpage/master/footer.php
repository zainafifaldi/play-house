		<div class="header_bottom"><!--header_bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-sm-push-6">
						Please read the <a href="#terms-modal" data-toggle="modal">Terms and Conditions</a> also <a href="#policy-modal" data-toggle="modal">Privacy Policy</a> to use this application.
					</div>
				</div>
			</div>
		</div><!--/header_bottom-->
		
		<?php $this->load->view("terms_and_condition");?>
		<?php $this->load->view("privacy_policy");?>
		
		<script src="<?=base_url("assets/js/moment.js")?>"></script>
		<script src="<?=base_url("assets/js/bootstrap.js")?>"></script>
		<script src="<?=base_url("assets/js/select2.js")?>"></script>
		<script src="<?=base_url("assets/js/bootstrap-datetimepicker.min.js")?>"></script>
		<script src="<?=base_url("assets/js/price-range.js")?>"></script>
    <script src="<?=base_url("assets/js/jquery.prettyPhoto.js")?>"></script>
    <script src="<?=base_url("assets/js/main.js")?>"></script>
	</body>
</html>