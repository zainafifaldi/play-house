<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<div class="col-md-12 search-sidebar">
						<h2>FILTER</h2>
						<div class="panel-group" id="accordian"><!--category-productsr-->
							<form action="<?=site_url("page/search_result")?>" method="GET">
								<div class="form-group">
									<select name="loc" class="form-control" id="form-search-location">
										<?php
											$loc = isset($fill['loc']) ? $fill['loc'] : "";
											$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $loc));
										?>
									</select>
								</div>
								
								<div class="form-group">
									<select name="tp" class="form-control" id="form-search-type">
										<?php
											$tp = isset($fill['tp']) ? $fill['tp'] : "";
											$this->load->view("frontpage/ajax/form_type", array("type" => $type, "tp" => $tp))
										?>
									</select>
								</div>
								
								<div class="form-group">
									<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="- Choose Venue -">
										<?php
											$ven = isset($fill['ven']) ? $fill['ven'] : "";
											$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue, "ven" => $ven))
										?>
									</select>
								</div>
								<script>
									$("#form-search-location, #form-search-type").change(function () {
										$.ajax({
											type:'GET',
											url:'<?=site_url('page/form_get_venue')?>',
											data:{
														'location':$("#form-search-location").val(), 
														'type':$("#form-search-type").val()
													 },
											success:function(data){
												$("#form-search-venue").html(data);
											},
											error:function(data){
												$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
											}
										});
									});
								</script>
								
								<div class="form-group">
									<div class="input-group date" id="datepicker1">
										<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" value="<?=isset($fill['d']) ? $fill['d'] : "";?>" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
								
								<div class="form-group">
									<div class="well text-center">
										<input type="text" name="rt" class="span2" id="range-time" data-slider-value="[<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>]" value="<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>"/><br/>
										<b class="pull-left text-green">07:00</b> <b class="pull-right text-green">24:00</b>
									</div>
								</div>
								
								<div class="form-group">
									<button type="submit" class="btn btn-block">Show Result<i class="fa fa-play pull-right"></i></button>
								</div>
							</form>
						</div><!--/category-products-->
					</div>
				</div>
			</div>
			
			<div class="col-md-9">
				<div class="col-md-12 table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Venue</th>
								<th>Court</th>
								<th>Date</th>
								<th style="width:180px">Schedule</th>
								<th>State</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($order_list['result'] as $index => $row): ?>
								<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
								<tr>
									<td style="text-align:right;"><?=($index+1)?></td>
									<td><?=$row->RESERVATION?></td>
									<td><a href="<?=site_url("page/detail/{$row->VENUE_ID}/{$row->TYPE}")?>"><?=$row->NAME?></a></td>
									<td style="text-indent:-25px;padding-left:25px;"><img src="<?php
																if($row->TYPE == "1")
																	echo base_url("assets/images/assets/PH_ico_football.png");
																else if($row->TYPE == "2")
																	echo base_url("assets/images/assets/PH_ico_batminton.png");
																else if($row->TYPE == "3")
																	echo base_url("assets/images/assets/PH_ico_tennis.png");
																else if($row->TYPE == "4")
																	echo base_url("assets/images/assets/PH_ico_bowling.png");
																?>" class="image-circle-green" style="height:20px"/> <?=$row->COURT?></td>
									<td><?=date("d M Y", strtotime($row->BOOK_DATE))?></td>
									<td><?php
											foreach($schedule['result'] as $index => $time) {
												if($index != 0)
													echo " | ";
												
												echo date("H:i", strtotime($time->TIME));
											}
											?></td>
									<td style="text-align:right"><?php
											if($row->LOCK_STATE == 0)
												echo "<span class='label label-warning'>Canceled</span>";
											else if($row->LOCK_STATE == 1)
												echo "<span class='label label-primary'>Pending</span>";
											else if($row->LOCK_STATE == 2)
												echo "<span class='label label-success'>Done</span>";
											?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>