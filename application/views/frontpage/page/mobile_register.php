<div role="main" class="ui-content">
	<h2>Register</h2>
	<?php if($this->session->flashdata("register_error")){ ?>
		<div class="show-page-loading-msg" data-theme="c" data-textonly="true" data-textvisible="true"
				data-html="<span style='color:white'><?=$this->session->flashdata("register_error")?></span>" data-inline="true" style="display:none;"></div>
	<?php } ?>
	<form action="<?=site_url("page/register_proc")?>" method="POST" class="form-horizontal" data-ajax="false">
		<input type="hidden" name="redirect" value="<?=$redirect?>"/>
		<input type="hidden" name="mobile" value="true"/>
		<label class="col-md-4 control-label">Name</label>
		<input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?=$this->session->flashdata("first_name")?>" required/>
		<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?=$this->session->flashdata("last_name")?>" required/>
		<label class="col-md-4 control-label">Username</label>
		<input type="text" name="username" class="form-control" placeholder="Your Login ID" value="<?=$this->session->flashdata("username")?>" required/>
		<label class="col-md-4 control-label">Identity<br/><small>(KTP/SIM/Passport)</small></label>
		<input type="text" name="identity" class="form-control" placeholder="Your Identity" value="<?=$this->session->flashdata("identity")?>" required/>
		<label class="col-md-4 control-label">Email</label>
		<input type="email" name="email" class="form-control" placeholder="Please type your correct email.." value="<?=$this->session->flashdata("email")?>" required/>
		<label class="col-md-4 control-label">Phone</label>
		<input type="text" name="phone" class="form-control" placeholder="ex: 08xx2100xxxx" value="<?=$this->session->flashdata("phone")?>" required/>
		<hr/>
		<label class="col-md-4 control-label">Password</label>
		<input type="password" name="password" class="form-control" placeholder="min. 6 character" required/>
		<label class="col-md-4 control-label">Confirm Password</label>
		<input type="password" name="repassword" class="form-control" placeholder="Re-type your password" required/>
		<fieldset data-role="controlgroup">
			<input type="checkbox" name="terms" id="checkbox-terms" required/>
			<label for="checkbox-terms">I have read and agree with Terms and Condition.</label>
		</fieldset>
		<button type="submit" class="btn pull-right">Register Now!</button>
	</form>
</div><!-- /content -->	