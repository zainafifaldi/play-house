<div role="main" class="ui-content">
	<h2>Your Book Detail</h2>
	<?php if($this->session->flashdata("book_error")){ ?>
		<div class="show-page-loading-msg" data-theme="c" data-textonly="true" data-textvisible="true"
				data-html="<span style='color:white'><?=$this->session->flashdata("book_error")?></span>" data-inline="true" style="display:none;"></div>
	<?php } ?>
	
	<form action="<?=site_url("page/book_proc")?>" method="POST" class="form-horizontal">
			<label class="col-md-4 control-label">Name: <?=$this->session->userdata("name")?></label>
			<label class="col-md-4 control-label">Email: <?=$this->session->userdata("email")?></label>
			<label class="col-md-4 control-label">Phone: <?=$this->session->userdata("phone")?></label>
		<hr/>
		<label class="col-md-4 control-label">Book Date: <?=date("F dS, Y", strtotime($date))?></label>
		<label style="float:right">@ RM <?=number_format($court['result']->PRICE,2,".",",");?> / <?=$court['result']->PER ? $court['result']->PER : "1/2";?> Hour</label>
		<label class="col-md-4 control-label">Schedule</label>
		<fieldset data-role="controlgroup">
			<?php
				foreach($availableDate['result'] as $index => $row) {
					echo "<input type='checkbox' name='time[]' id='cb-{$row->ID}'";
					if($scheduleId == $row->ID)
						echo " checked='checked'";
					echo " value='{$row->ID}'/> ";
					echo "<label for='cb-{$row->ID}'>". date("H:i", strtotime($row->TIME)) ."</label>";
				}
			?>
		</fieldset>
		<input type="hidden" name="date" value="<?=$date?>"/>
		<button type="submit" class="btn">Book Now!</button>
	</form>
</div>