<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<div class="col-md-12 search-sidebar">
						<h2>FILTER</h2>
						<div class="panel-group" id="accordian"><!--category-productsr-->
							<form action="<?=site_url("page/search_result")?>" method="GET">
								<div class="form-group">
									<select name="loc" class="form-control" id="form-search-location">
										<?php
											$loc = isset($fill['loc']) ? $fill['loc'] : "";
											$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $loc));
										?>
									</select>
								</div>
								
								<div class="form-group">
									<select name="tp" class="form-control" id="form-search-type">
										<?php
											$tp = isset($fill['tp']) ? $fill['tp'] : "";
											$this->load->view("frontpage/ajax/form_type", array("type" => $type, "tp" => $tp))
										?>
									</select>
								</div>
								
								<div class="form-group">
									<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="- Choose Venue -">
										<?php
											$ven = isset($fill['ven']) ? $fill['ven'] : "";
											$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue, "ven" => $ven))
										?>
									</select>
								</div>
								<script>
									$("#form-search-location, #form-search-type").change(function () {
										$.ajax({
											type:'GET',
											url:'<?=site_url('page/form_get_venue')?>',
											data:{
														'location':$("#form-search-location").val(), 
														'type':$("#form-search-type").val()
													 },
											success:function(data){
												$("#form-search-venue").html(data);
											},
											error:function(data){
												$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
											}
										});
									});
								</script>
								
								<div class="form-group">
									<div class="input-group date" id="datepicker1">
										<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" value="<?=isset($fill['d']) ? $fill['d'] : "";?>" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
								
								<div class="form-group">
									<div class="well text-center">
										<input type="text" name="rt" class="span2" id="range-time" data-slider-value="[<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>]" value="<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>"/><br/>
										<b class="pull-left text-green">07:00</b> <b class="pull-right text-green">24:00</b>
									</div>
								</div>
								
								<div class="form-group">
									<button type="submit" class="btn btn-block">Show Result<i class="fa fa-play pull-right"></i></button>
								</div>
							</form>
						</div><!--/category-products-->
					</div>
				</div>
			</div>
			
			<div class="col-sm-9 padding-right">
				<div class="features_items"><!--features_items-->
					<div class="title-box">
						<h2 class="title">Search Result</h2>
					</div>
					<ul class="content-box">
						<?php
							if(!$court_strict['is_error']):
								foreach($court_strict['result'] as $row):
						?>
											<li>
												<div class='col-md-4'>
													<div class='result-photo'>
														<img src="<?=$row->PHOTO ? base_url("photo/gallery/". $row->PHOTO) : base_url("photo/gallery/DEFAULT.jpg")?>" alt="<?=$row->COURT_NAME?>" class="img-responsive" align="middle"/>
													</div>
												</div>
												<div class='col-md-8 result-detail'>
													<div class="col-md-9">
														<h3><?=$row->NAME?></h3>
														<span>
															<img src="<?php
																					if($row->TYPE_NAME == "Futsal") echo base_url("assets/images/assets/PH_ico_football.png");
																					else if($row->TYPE_NAME == "Badminton") echo base_url("assets/images/assets/PH_ico_batminton.png");
																					else if($row->TYPE_NAME == "Tenis") echo base_url("assets/images/assets/PH_ico_tennis.png");
																					else if($row->TYPE_NAME == "Ping Pong") echo base_url("assets/images/assets/PH_ico_bowling.png");
																				?>" alt="<?=$row->TYPE_NAME?>" class="type-icon black-tooltip" title="<?=$row->TYPE_NAME?>" data-toggle="tooltip"/> |
															<?=$row->CITY?> (<?=$row->COUNTRY?>)</span>
														<h4>
															<?php
															if($row->PRICE_FROM == $row->PRICE_TO)
																echo "RM " . number_format($row->PRICE_FROM,0,".",",") , "<br/>";
															else
																echo "RM " . number_format($row->PRICE_FROM,0,".",",") . " - RM " . number_format($row->PRICE_TO,0,".",",") . "<br/>";
															?>
														</h4>
													</div>
													<div class="col-md-3">
														<a href="<?=site_url("page/detail/{$row->VENUE_ID}/{$row->TYPE_ID}?d={$fill['d']}&rt={$fill['rt']}")?>" class="btn btn-block">Check&nbsp;&nbsp;<i class="fa fa-play text-green"></i></a>
													</div>
												</div>
											</li>
						<?php
								endforeach;
							endif;
						?>
						
						<?php
							if(!$court_tolerant['is_error']):
								foreach($court_tolerant['result'] as $row):
						?>
											<li>
												<div class='col-md-4'>
													<div class='result-photo'>
														<img src="<?=$row->PHOTO ? base_url("photo/gallery/". $row->PHOTO) : base_url("photo/gallery/DEFAULT.jpg")?>" alt="<?=$row->COURT_NAME?>" class="img-responsive" align="middle"/>
													</div>
												</div>
												<div class='col-md-8 result-detail'>
													<div class="col-md-9">
														<h3><?=$row->NAME?></h3>
														<span>
															<img src="<?php
																					if($row->TYPE_NAME == "Futsal") echo base_url("assets/images/assets/PH_ico_football.png");
																					else if($row->TYPE_NAME == "Badminton") echo base_url("assets/images/assets/PH_ico_batminton.png");
																					else if($row->TYPE_NAME == "Tenis") echo base_url("assets/images/assets/PH_ico_tennis.png");
																					else if($row->TYPE_NAME == "Ping Pong") echo base_url("assets/images/assets/PH_ico_bowling.png");
																				?>" alt="<?=$row->TYPE_NAME?>" class="type-icon black-tooltip" title="<?=$row->TYPE_NAME?>" data-toggle="tooltip"/> |
															<?=$row->CITY?> (<?=$row->COUNTRY?>)</span>
														<h4>
															<?php
															if($row->PRICE_FROM == $row->PRICE_TO)
																echo "RM " . number_format($row->PRICE_FROM,0,".",",") , "<br/>";
															else
																echo "RM " . number_format($row->PRICE_FROM,0,".",",") . " - RM " . number_format($row->PRICE_TO,0,".",",") . "<br/>";
															?>
														</h4>
													</div>
													<div class="col-md-3">
														<a href="<?=site_url("page/detail/{$row->VENUE_ID}/{$row->TYPE_ID}?d={$fill['d']}&rt={$fill['rt']}")?>" class="btn btn-block">Check&nbsp;&nbsp;<i class="fa fa-play text-green"></i></a>
													</div>
												</div>
											</li>
						<?php
								endforeach;
							endif;
							
							if($court_strict['is_error'] && $court_tolerant['is_error'])
								echo "<p class='col-md-12'>No result found.</p>";
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>