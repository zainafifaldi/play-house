<div role="main" class="ui-content">
	<h2 class="title">Search Result</h2>
	
	<ul data-role="listview" data-split-theme="b" data-inset="true">
		<?php
			if(!$court_strict['is_error']):
				if(!isset($fill['rt']))
					$fill['rt'] = $fill['rt-from'] .",". $fill['rt-to'];
				
				foreach($court_strict['result'] as $row):
		?>
					<li>
						<a href="<?=site_url("page/m/detail/{$row->VENUE_ID}/{$row->TYPE_ID}?d={$fill['d']}&rt={$fill['rt']}")?>">
							<img src="<?=$row->PHOTO ? base_url("photo/gallery/". $row->PHOTO) : base_url("photo/gallery/DEFAULT.jpg")?>" alt="<?=$row->COURT_NAME?>"/>
							<h2><?=$row->NAME?></h2>
							<p><?=$row->CITY?> (<?=$row->COUNTRY?>)</p>
							<p>
								<strong>
								<?php
									if($row->PRICE_FROM == $row->PRICE_TO)
										echo "RM " . number_format($row->PRICE_FROM,2,".",",") , "<br/>";
									else
										echo "RM " . number_format($row->PRICE_FROM,2,".",",") . " - RM " . number_format($row->PRICE_TO,2,".",",") . "<br/>";
								?>
								</strong>
							</p>
							
							<p class="ui-li-aside"><strong><img src="<?php
																	if($row->TYPE_NAME == "Futsal") echo base_url("assets/images/assets/PH_ico_football.png");
																	else if($row->TYPE_NAME == "Badminton") echo base_url("assets/images/assets/PH_ico_batminton.png");
																	else if($row->TYPE_NAME == "Tenis") echo base_url("assets/images/assets/PH_ico_tennis.png");
																	else if($row->TYPE_NAME == "Ping Pong") echo base_url("assets/images/assets/PH_ico_bowling.png");
																?>" alt="<?=$row->TYPE_NAME?>" height="15px"/></strong></p>
						</a>
						<a href="<?=site_url("page/m/detail/{$row->VENUE_ID}/{$row->TYPE_ID}?d={$fill['d']}&rt={$fill['rt']}")?>">Purchase album</a>
					</li>
		<?php
				endforeach;
			endif;
		?>
		
		<?php
			if(!$court_tolerant['is_error']):
				foreach($court_tolerant['result'] as $row):
		?>
					<li>
						<a href="#">
							<img src="<?=$row->PHOTO ? base_url("photo/gallery/". $row->PHOTO) : base_url("photo/gallery/DEFAULT.jpg")?>" alt="<?=$row->COURT_NAME?>"/>
							<h2><?=$row->NAME?></h2>
							<p><?=$row->CITY?> (<?=$row->COUNTRY?>)</p>
							<p>
								<strong>
								<?php
									if($row->PRICE_FROM == $row->PRICE_TO)
										echo "RM " . number_format($row->PRICE_FROM,2,".",",") , "<br/>";
									else
										echo "RM " . number_format($row->PRICE_FROM,2,".",",") . " - RM " . number_format($row->PRICE_TO,2,".",",") . "<br/>";
								?>
								</strong>
							</p>
							
							<p class="ui-li-aside"><strong><img src="<?php
																	if($row->TYPE_NAME == "Futsal") echo base_url("assets/images/assets/PH_ico_football.png");
																	else if($row->TYPE_NAME == "Badminton") echo base_url("assets/images/assets/PH_ico_batminton.png");
																	else if($row->TYPE_NAME == "Tenis") echo base_url("assets/images/assets/PH_ico_tennis.png");
																	else if($row->TYPE_NAME == "Ping Pong") echo base_url("assets/images/assets/PH_ico_bowling.png");
																?>" alt="<?=$row->TYPE_NAME?>" height="15px"/></strong></p>
						</a>
						<a href="<?=site_url("page/m/detail/{$row->VENUE_ID}/{$row->TYPE_ID}?d={$fill['d']}&rt={$fill['rt']}")?>">Purchase album</a>
					</li>
		<?php
				endforeach;
			endif;
			
			if($court_strict['is_error'] && $court_tolerant['is_error'])
				echo "<p class='col-md-12'>No result found.</p>";
		?>
	</ul>
</div><!-- /content -->	