<form action="<?=site_url("page/search_result")?>" method="GET">
	<div class="form-group">
		<select name="loc" class="form-control" id="form-search-location">
			<?php
				$this->load->view("frontpage/ajax/form_location", array("location" => $location))
			?>
		</select>
	</div>
	
	<div class="form-group">
		<select name="tp" class="form-control" id="form-search-type">
			<?php
				$this->load->view("frontpage/ajax/form_type", array("type" => $type))
			?>
		</select>
	</div>
	
	<div class="form-group">
		<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="Choose Venue">
			<?php
				$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue))
			?>
		</select>
	</div>
	<script>
		$("#form-search-location, #form-search-type").change(function () {
			$.ajax({
				type:'GET',
				url:'<?=site_url('page/form_get_venue')?>',
				data:{
							'location':$("#form-search-location").val(), 
							'type':$("#form-search-type").val()
						 },
				success:function(data){
					$("#form-search-venue").html(data);
				},
				error:function(data){
					$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
				}
			});
		});
	</script>
	
	<div class="form-group">
		<div class="input-group date" id="datepicker1">
			<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" />
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
	
	<div class="form-group">
		<div class="well text-center">
			<input type="text" name="rt" class="span2" id="range-time"/><br/>
			<b class="pull-left">07:00</b> <b class="pull-right">24:00</b>
		</div>
	</div>
	
	<input type="submit"/>
</form>