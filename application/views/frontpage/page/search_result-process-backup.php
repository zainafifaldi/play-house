<form action="<?=site_url("page/search_result")?>" method="GET">
	<div class="form-group">
		<select name="loc" class="form-control" id="form-search-location">
			<?php
				$loc = isset($fill['loc']) ? $fill['loc'] : "";
				$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $loc));
			?>
		</select>
	</div>
	
	<div class="form-group">
		<select name="tp" class="form-control" id="form-search-type">
			<?php
				$tp = isset($fill['tp']) ? $fill['tp'] : "";
				$this->load->view("frontpage/ajax/form_type", array("type" => $type, "tp" => $tp))
			?>
		</select>
	</div>
	
	<div class="form-group">
		<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="Choose Venue">
			<?php
				$ven = isset($fill['ven']) ? $fill['ven'] : "";
				$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue, "ven" => $ven))
			?>
		</select>
	</div>
	<script>
		$("#form-search-location, #form-search-type").change(function () {
			$.ajax({
				type:'GET',
				url:'<?=site_url('page/form_get_venue')?>',
				data:{
							'location':$("#form-search-location").val(), 
							'type':$("#form-search-type").val()
						 },
				success:function(data){
					$("#form-search-venue").html(data);
				},
				error:function(data){
					$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
				}
			});
		});
	</script>
	
	<div class="form-group">
		<div class="input-group date" id="datepicker1">
			<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" />
			<span class="input-group-addon">
				<span class="glyphicon glyphicon-calendar"></span>
			</span>
		</div>
	</div>
	
	<div class="form-group">
		<input name="rt" id="range-time" type="text"/>
	</div>
	
	<input type="submit"/>
</form>


<ul>
	<?php
		if(!$court['is_error']) {
			foreach($court['result'] as $row) {
				echo "<li>
							{$row->NAME} ({$row->COURT_NAME})<br/>
							{$row->CITY} ({$row->COUNTRY})<br/>
							{$row->TYPE_NAME}<br/>";
						if($row->PRICE_FROM == $row->PRICE_TO)
							echo "Rp " . number_format($row->PRICE_FROM,0,",",".") , "<br/>";
						else
							echo "Rp " . number_format($row->PRICE_FROM,0,",",".") . " - Rp " . number_format($row->PRICE_TO,0,",",".") . "<br/>";
				echo "	<a href=\"". site_url("page/detail/{$row->VENUE_ID}/{$row->TYPE_ID}") ."\">Check</a>
							</li>";
			}
		}
		else {
			echo "Tidak ditemukan";
		}
	?>
</ul>