<div role="main" class="ui-content">
	<?php if(!$venue['is_error']): ?>
		<div class="swiper-container">
        <div class="swiper-wrapper">
						<?php
							// MAIN PHOTO
							$photoIteration = 0;
							foreach($court['result'] as $row) {
								if($row->PHOTO) {
									echo "<div class=\"swiper-slide\">
													<img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\" style=\"width:100%; max-width:2048px; margin:0 auto; display:block;\" alt=\"\"/>
												</div>";
									
									++ $photoIteration;
								}
							}
							
							if($photoIteration == 0) {
								echo "<div class=\"swiper-slide\">
												<img src=\"". base_url("photo/gallery/DEFAULT.jpg") ."\" class=\"img-responsive\" alt=\"\"/>
											</div>";
							}
						?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
		<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
    </script>
		<div id="map" style="height:200px;width:100%;"></div>
		<script>
			var map, marker, position = {lat: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LAT : $venue['result']->MAPS_LAT_LOC;?>, lng: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LNG : $venue['result']->MAPS_LNG_LOC;?>};
			function initMap() {
					map = new google.maps.Map(document.getElementById('map'), {
							center: position,
							zoom  : <?=$venue['result']->MAPS_LAT ? 15 : 11?>,
							styles:[
											{
													"elementType": "geometry",
													"stylers": [
															{
																	"hue": "#ff4400"
															},
															{
																	"saturation": -68
															},
															{
																	"lightness": -4
															},
															{
																	"gamma": 0.72
															}
													]
											},
											{
													"featureType": "road",
													"elementType": "labels.icon"
											},
											{
													"featureType": "landscape.man_made",
													"elementType": "geometry",
													"stylers": [
															{
																	"hue": "#0077ff"
															},
															{
																	"gamma": 3.1
															}
													]
											},
											{
													"featureType": "water",
													"stylers": [
															{
																	"hue": "#00ccff"
															},
															{
																	"gamma": 0.44
															},
															{
																	"saturation": -33
															}
													]
											},
											{
													"featureType": "poi.park",
													"stylers": [
															{
																	"hue": "#44ff00"
															},
															{
																	"saturation": -23
															}
													]
											},
											{
													"featureType": "water",
													"elementType": "labels.text.fill",
													"stylers": [
															{
																	"hue": "#007fff"
															},
															{
																	"gamma": 0.77
															},
															{
																	"saturation": 65
															},
															{
																	"lightness": 99
															}
													]
											},
											{
													"featureType": "water",
													"elementType": "labels.text.stroke",
													"stylers": [
															{
																	"gamma": 0.11
															},
															{
																	"weight": 5.6
															},
															{
																	"saturation": 99
															},
															{
																	"hue": "#0091ff"
															},
															{
																	"lightness": -86
															}
													]
											},
											{
													"featureType": "transit.line",
													"elementType": "geometry",
													"stylers": [
															{
																	"lightness": -48
															},
															{
																	"hue": "#ff5e00"
															},
															{
																	"gamma": 1.2
															},
															{
																	"saturation": -23
															}
													]
											},
											{
													"featureType": "transit",
													"elementType": "labels.text.stroke",
													"stylers": [
															{
																	"saturation": -64
															},
															{
																	"hue": "#ff9100"
															},
															{
																	"lightness": 16
															},
															{
																	"gamma": 0.47
															},
															{
																	"weight": 2.7
															}
													]
											}
									]
					});
					<?php if($venue['result']->MAPS_LAT) { ?>
					marker = new google.maps.Marker({
							position : position,
							map      : map,
							title    : '<?=$venue['result']->NAME?>'
					});
					<?php } ?>
			}
			
			google.maps.event.addDomListener(window, 'load', initMap);
		</script>
		
		<h2 class="title"><?=$venue['result']->NAME;?> <small>| <?=$type['result']->TYPE?></small></h2>
		<p><?=$venue['result']->DESCRIPTION?></p>
		
		<hr/>
		<h3>Suggestion Date</h3>
		<?php
			$av = $av ? $av : date("d/m/Y");
			$avParse = explode("/", $av);
			if(!isset($avParse[1])) {
				$avParse = explode("-", $av);
				$temp = $avParse[0];
				$avParse[0] = $avParse[2];
				$avParse[2] = $temp;
			}
		?>
		
		<ul data-role="listview" data-inset="true">
			<?php foreach($availableStrict['result'] as $index => $row) : ?>
			<?php
				$day = date("D", strtotime($date));
				$schedule = $this->Venuemodel->get_schedule_detail($venue['result']->VENUE_ID, $row->COURT_NAME, "$from:00", $day);
				if(!$schedule['is_error']) :
			?>
			<li><a href="<?=site_url("page/m/start_book/{$schedule['result']->SCHEDULE_ID}/$date")?>">
				<h2><?=$row->COURT_NAME?></h2>
				<p><strong>RM <?=number_format($row->PRICE, 2, ".", ",")?></strong></p>
				<p><?=date("H:i", strtotime($schedule['result']->TIME))?> at <?=date("F dS", strtotime($date))?></p>
			</a></li>
			<?php endif; ?>
			<?php endforeach; ?>
			
			<?php if(!$availableStrict2['is_error']): ?>
			<?php foreach($availableStrict2['result'] as $index => $row) : ?>
			<?php
				$day = date("D", strtotime("$avParse[2]-$avParse[1]-$avParse[0]"));
				$schedule = $this->Venuemodel->get_schedule_detail($venue['result']->VENUE_ID, $row->COURT_NAME, "$from:00", $day);
			?>
			<li><a href="<?=site_url("page/m/start_book/{$schedule['result']->SCHEDULE_ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>">
				<h2><?=$row->COURT_NAME?></h2>
				<p><strong>RM <?=number_format($row->PRICE, 2, ".", ",")?></strong></p>
				<p><?=date("H:i", strtotime($schedule['result']->TIME))?> at <?=date("F dS", strtotime("$avParse[2]-$avParse[1]-$avParse[0]"))?></p>
			</a></li>
			<?php endforeach; ?>
			<?php endif; ?>
		</ul>
		
		<hr/>
		
		<h3>Available Date</h3>
		<?php
			$dateParse = explode("-", $date);
		?>
		<form action="" method="GET" id="form-av">
			<input type="hidden" name="d" value="<?="$dateParse[2]/$dateParse[1]/$dateParse[0]"?>"/>
			<input type="hidden" name="rt" value="<?="$from,$to"?>"/>
			<input type="date" name="av" value="<?=$av ? $av : date("d/m/Y")?>" />
			<input type="submit" value="Check"/>
		</form>
		
		<style>
			.ui-table-columntoggle-btn.ui-btn.ui-btn-a.ui-corner-all.ui-shadow.ui-mini {
				display:none;
			}
		</style>
		<table data-role="table" id="table-custom-2" data-mode="columntoggle" class="ui-body-b ui-shadow table-stripe ui-responsive">
			<thead>
				<tr>
					<th>Court</th>
					<th style="width:23%">Price</th>
					<th style="width:50%">Available Time</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$courtName = "";
				foreach($availableDate['result'] as $index => $row){
					if($row->COURT_NAME != $courtName) {
						if($index != 0) {
							echo "	</td>
										</tr>";
						}
				?>
					<tr>
						<td><?=$row->COURT_NAME?></td>
						<td>RM <?=number_format($row->PRICE, 2, ".", ",")?></td>
						<td><a data-toggle="tooltip" title="Book This" href="<?=site_url("page/m/start_book/{$row->ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>"><?=date("H:i", strtotime($row->TIME))?></a>
				<?php
						$courtName = $row->COURT_NAME;
					}
					else {
						?>| <a data-toggle="tooltip" title="Book This" href="<?=site_url("page/m/start_book/{$row->ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>"><?=date("H:i", strtotime($row->TIME))?></a> <?php
					}
					
					if($index == (count($availableDate['result']) - 1)) {
						echo "	</td>
									</tr>";
					}
				}
				?>
			</tbody>
		</table>
	<?php else: ?>
		<h2 class="title">Not Found</h2>
	<?php endif; ?>
</div>