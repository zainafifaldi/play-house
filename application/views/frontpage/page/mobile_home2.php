<script>
$(document).ready(function(){
if(screen.width < 720) {
	$(".ui-mobile").html("");
}
else {
	$(".ui-desktop").html("");
}
	
});
</script>
<div role="main" class="ui-content ui-mobile">
	<form action="<?=site_url("page/search_result")?>" method="GET" class="form-horizontal">
		<label for="form-search-location" class="select">Location</label>
		<select name="loc" id="form-search-location" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_location", array("location" => $location))
			?>
		</select>
		
		<label for="form-search-type" class="select">Type</label>
		<select name="tp" id="form-search-type" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_type", array("type" => $type))
			?>
		</select>
		
		<label for="form-search-venue" class="select">Venue</label>
		<select name="ven" id="form-search-venue" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue))
			?>
		</select>
		<script>
			$("#form-search-location, #form-search-type").change(function () {
				$.ajax({
					type:'GET',
					url:'<?=site_url('page/form_get_venue')?>',
					data:{
								'location':$("#form-search-location").val(), 
								'type':$("#form-search-type").val()
							 },
					success:function(data){
						$("#form-search-venue").html(data);
					},
					error:function(data){
						$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
					}
				});
			});
		</script>
		
		<label for="form-search-date" class="">Book Date</label>
		<input type="date" name="d" id="form-search-date" value="" />
		
		<div data-role="rangeslider" data-mini="true">
			<label for="range-2a" class="">Available In</label>
			<input type="range" name="rt-from" id="range-2a" min="0" max="24" value="13">
			<label for="range-2b" class="">Available In</label>
			<input type="range" name="rt-to" id="range-2b" min="0" max="24" value="15">
    </div>
		
		<button type="submit" class="ui-btn ui-mini ui-icon-search ui-btn-icon-left">Find It</button>
	</form>
</div><!-- /content -->

<div class="ui-desktop">
<?php if($this->session->flashdata("success")){ ?>
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
		<?=$this->session->flashdata("success")?>
	</div>
<?php } ?>

<?php if($this->session->flashdata("error")){ ?>
	<div class="alert alert-danger">
		<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
		<?=$this->session->flashdata("error")?>
	</div>
<?php } ?>

<section id="slider"><!--slider-->
	<!--<div class="container">-->
		<div class="row">
			<div class="col-sm-12">
				<div id="slider-carousel" class="carousel horizontal slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<?php
						if($court['is_error'] || count($court['result']) <= 1)
							$court = $courtOptional;

						$marger = count($court['result']) > 3 ? 3 : count($court['result']);
						$courtSlider = array_rand($court['result'], $marger);
						shuffle($courtSlider);
						
						foreach($courtSlider as $index => $row) {
							echo '<li data-target="#slider-carousel" data-slide-to="'. $index .'" class="';
							echo $index ? "" : "active";
							echo '"></li>';
						}
						?>
					</ol>
					
					<div class="carousel-inner">
						<?php foreach($courtSlider as $index => $row): ?>
							<div class="item <?=$index ? "" : "active"?>">
								<div class="background-photo">
									<img src="<?=base_url("photo/gallery/")?>/<?=$court['result'][$row]->PHOTO ? $court['result'][$row]->PHOTO : "DEFAULT.jpg"?>" class="img-responsive" alt=""/>
								</div>
								<div class="container">
									<div class="col-md-5">
										<h1><?=$court['result'][$row]->NAME?></h1>
										<h2><?=$court['result'][$row]->TYPE_NAME?> - From RM <?=number_format($court['result'][$row]->PRICE_FROM, 2, ".", ",")?></h2>
										<p><?=$court['result'][$row]->DESCRIPTION?></p>
										<a href="<?=site_url("page/detail/". $court['result'][$row]->VENUE_ID ."/". $court['result'][$row]->TYPE)?>" class="btn btn-default get"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;BOOK NOW</a>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						
					</div>
					
					<div class="carousel-search">
						<div class="container">
							<div class="col-md-7 col-md-push-5">
								<div class="search-venue-form front">
									<div class="col-md-8 col-md-push-2">
										<form action="<?=site_url("page/search_result")?>" method="GET" class="form-horizontal">
											<div class="form-group">
												<label class="col-md-3 control-label" style="text-align:left">Location</label>
												<div class="col-md-9">
													<select name="loc" class="form-control" id="form-search-location">
														<?php
															$this->load->view("frontpage/ajax/form_location", array("location" => $location))
														?>
													</select>
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label" style="text-align:left">Type</label>
												<div class="col-md-9">
													<select name="tp" class="form-control" id="form-search-type">
														<?php
															$this->load->view("frontpage/ajax/form_type", array("type" => $type))
														?>
													</select>
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-3 control-label" style="text-align:left">Venue</label>
												<div class="col-md-9">
													<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="Choose Venue">
														<?php
															$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue))
														?>
													</select>
												</div>
											</div>
											<script>
												$("#form-search-location, #form-search-type").change(function () {
													$.ajax({
														type:'GET',
														url:'<?=site_url('page/form_get_venue')?>',
														data:{
																	'location':$("#form-search-location").val(), 
																	'type':$("#form-search-type").val()
																 },
														success:function(data){
															$("#form-search-venue").html(data);
														},
														error:function(data){
															$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
														}
													});
												});
											</script>
											
											<div class="form-group">
												<label class="col-md-4 control-label" style="text-align:left">Book Date</label>
												<div class="col-md-8">
													<div class="input-group date" id="datepicker1">
														<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" />
														<span class="input-group-addon">
															<span class="glyphicon glyphicon-calendar"></span>
														</span>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<label class="col-md-4 control-label" style="text-align:left">Available In</label>
												<div class="col-md-8">
													<div class="well text-center">
														<input type="text" name="rt" class="span2" id="range-time"/><br/>
														<b class="pull-left text-green">07:00</b> <b class="pull-right text-green">24:00</b>
													</div>
												</div>
											</div>
											
											<div class="form-group">
												<div class="col-md-10 col-md-push-2">
													<button type="submit" class="btn"><i class="fa fa-search"></i>&nbsp;&nbsp;Find It</button>
												</div>
											</div>
										</form>
										
									</div>
								</div>
							</div>
						</div>
					</div>
					<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
						<i class="fa fa-angle-left"></i>
					</a>
					<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
						<i class="fa fa-angle-right"></i>
					</a>
				</div>
				
			</div>
		</div>
	<!--</div>-->
	
	<div class="row">
		<div class="green-box"></div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="container">
				<div id="carousel-vertical" class="carousel vertical slide">
					<div class="carousel-inner" role="listbox">
						<?php foreach($ads['result'] as $index => $row): ?>
							<div class="item <?=$index ? "" : "active";?>">
								<p class="ticker-headline">
									<a href="<?=$row->LINK ? $row-> LINK : "#"?>" <?=$row->LINK ? "target='_blank'" : ""?>>
										<strong><?=$row->HEADLINE?></strong> <?=$row->CONTENT?>
									</a>
								</p>
							</div>
						<?php endforeach; ?>
					</div>
				</div>

				<!-- Controls -->
				<a class="up carousel-controls" href="#carousel-vertical" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
				</a>
				<a class="down carousel-controls" href="#carousel-vertical" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
				</a>
			</div>
		</div>
	</div>
</section><!--/slider-->
</div>