<div class="container">
	<div class="row">
		<div class="col-md-4 form-box">
			<h2>Sign In Here</h2>
			<hr/>
			
			<?php if($this->session->flashdata("login_error")){ ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
					<?=$this->session->flashdata("login_error")?>
				</div>
			<?php } ?>

			<form action="<?=site_url("page/login_proc")?>" method="POST" class="form-horizontal">
				<input type="hidden" name="redirect" value="<?=$redirect?>"/>
				<div class="form-group">
					<label class="col-md-4 control-label">Username</label>
					<div class="col-md-8">
						<input type="text" name="login_username" class="form-control" placeholder="Your Login ID" value="<?=$this->session->flashdata("login_username")?>" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-8">
						<input type="password" name="password" class="form-control" placeholder="Your Password" required/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-push-4">
						<button type="submit" class="btn">Sign In</button>
					</div>
				</div>
			</form>
		</div>
		
		<div class="col-md-7 col-md-push-1 form-box">
			<h2>Register <span class="text-green">Free</span>
			<small>If you not have account</small></h2>
			<hr/>
			
			<?php if($this->session->flashdata("register_error")){ ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
					<?=$this->session->flashdata("register_error")?>
				</div>
			<?php } ?>


			<form action="<?=site_url("page/register_proc")?>" method="POST" class="form-horizontal">
				<input type="hidden" name="redirect" value="<?=$redirect?>"/>
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-3">
						<input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?=$this->session->flashdata("first_name")?>" required/>
					</div>
					<div class="col-md-5">
						<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?=$this->session->flashdata("last_name")?>" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Username</label>
					<div class="col-md-8">
						<input type="text" name="username" class="form-control" placeholder="Your Login ID" value="<?=$this->session->flashdata("username")?>" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Identity<br/><small>(KTP/SIM/Passport)</small></label>
					<div class="col-md-8">
						<input type="text" name="identity" class="form-control" placeholder="Your Identity" value="<?=$this->session->flashdata("identity")?>" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input type="email" name="email" class="form-control" placeholder="Please type your correct email.." value="<?=$this->session->flashdata("email")?>" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Phone</label>
					<div class="col-md-8">
						<input type="text" name="phone" class="form-control" placeholder="ex: 08xx2100xxxx" value="<?=$this->session->flashdata("phone")?>" required/>
					</div>
				</div>
				<hr/>
				<div class="form-group">
					<label class="col-md-4 control-label">Password</label>
					<div class="col-md-8">
						<input type="password" name="password" class="form-control" placeholder="min. 6 character" required/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Confirm Password</label>
					<div class="col-md-8">
						<input type="password" name="repassword" class="form-control" placeholder="Re-type your password" required/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-push-4">
						<input type="checkbox" name="terms" required/> I have read and agree with <a href="#terms-modal" data-toggle="modal">Terms and Condition</a>.
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<button type="submit" class="btn pull-right">Register Now!</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>