<div role="main" class="ui-content">
	<h2 class="title">Your Order</h2>
	<ul data-role="listview" data-split-theme="b" data-inset="true">
		<?php foreach($order_list['result'] as $index => $row): ?>
			<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
			<li>
				<a href="<?=site_url("page/m/detail/{$row->VENUE_ID}/{$row->TYPE}")?>">
					<h2><?=$row->RESERVATION?></h2>
					<p><?=$row->NAME?> | <?=$row->TYPE_NAME;?> <?=$row->COURT?></p>
					<p><?=date("d M Y", strtotime($row->BOOK_DATE))?></p>
					<p><?php
							foreach($schedule['result'] as $index => $time) {
								if($index != 0)
									echo " | ";
								
								echo date("H:i", strtotime($time->TIME));
							}
							?></p>
					<p class="ui-li-aside"><?php
								if($row->LOCK_STATE == 0)
									echo "<span class='label label-warning'>Canceled</span>";
								else if($row->LOCK_STATE == 1)
									echo "<span class='label label-primary'>Pending</span>";
								else if($row->LOCK_STATE == 2)
									echo "<span class='label label-success'>Done</span>";
								?></p>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>