<div class="container">
	<div class="row">
		<div class="col-md-3 form-box">
			<div class="left-sidebar">
				<div class="col-md-12 search-sidebar">
					<h2>FILTER</h2>
					<div class="panel-group" id="accordian"><!--category-productsr-->
						<form action="<?=site_url("page/search_result")?>" method="GET">
							<div class="form-group">
								<select name="loc" class="form-control" id="form-search-location">
									<?php
										$loc = isset($fill['loc']) ? $fill['loc'] : "";
										$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $loc));
									?>
								</select>
							</div>
							
							<div class="form-group">
								<select name="tp" class="form-control" id="form-search-type">
									<?php
										$tp = isset($fill['tp']) ? $fill['tp'] : "";
										$this->load->view("frontpage/ajax/form_type", array("type" => $type, "tp" => $tp))
									?>
								</select>
							</div>
							
							<div class="form-group">
								<select name="ven" class="form-control form-select2" id="form-search-venue" data-placeholder="- Choose Venue -">
									<?php
										$ven = isset($fill['ven']) ? $fill['ven'] : "";
										$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue, "ven" => $ven))
									?>
								</select>
							</div>
							<script>
								$("#form-search-location, #form-search-type").change(function () {
									$.ajax({
										type:'GET',
										url:'<?=site_url('page/form_get_venue')?>',
										data:{
													'location':$("#form-search-location").val(), 
													'type':$("#form-search-type").val()
												 },
										success:function(data){
											$("#form-search-venue").html(data);
										},
										error:function(data){
											$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
										}
									});
								});
							</script>
							
							<div class="form-group">
								<div class="input-group date" id="datepicker1">
									<input type="text" name="d" class="form-control" placeholder="dd/mm/yyyy" value="<?=isset($fill['d']) ? $fill['d'] : "";?>" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							
							<div class="form-group">
								<div class="well text-center">
									<input type="text" name="rt" class="span2" id="range-time" data-slider-value="[<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>]" value="<?=isset($fill['rt']) ? ($fill['rt'] ? $fill['rt'] : "7,24") : "7,24"?>"/><br/>
									<b class="pull-left text-green">07:00</b> <b class="pull-right text-green">24:00</b>
								</div>
							</div>
							
							<div class="form-group">
								<button type="submit" class="btn btn-block">Show Result<i class="fa fa-play pull-right"></i></button>
							</div>
						</form>
					</div><!--/category-products-->
				</div>
			</div>
		</div>
		
		<div class="col-md-8 col-md-push-1 form-box">
			<h2>Your <span class="text-green">Book Detail</span>
			<small>Please check your data</small></h2>
			<hr/>
			
			<?php if($this->session->flashdata("book_error")){ ?>
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
					<?=$this->session->flashdata("book_error")?>
				</div>
			<?php } ?>


			<form action="<?=site_url("page/book_proc")?>" method="POST" class="form-horizontal">
				<div class="form-group">
					<label class="col-md-4 control-label">Name</label>
					<div class="col-md-8">
						<input type="text" class="form-control" value="<?=$this->session->userdata("name")?>" disabled="disabled"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Email</label>
					<div class="col-md-8">
						<input type="text" class="form-control" value="<?=$this->session->userdata("email")?>" disabled="disabled"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Phone</label>
					<div class="col-md-8">
						<input type="text" class="form-control" value="<?=$this->session->userdata("phone")?>" disabled="disabled"/>
					</div>
				</div>
				<hr/>
				<div class="form-group">
					<label class="col-md-4 control-label">Book Date</label>
					<div class="col-md-8">
						<p class="form-control-static"><?=date("F dS, Y", strtotime($date))?> <span class="pull-right">@ RM <?=number_format($court['result']->PRICE,2,".",",");?> / <?=$court['result']->PER ? $court['result']->PER : "1/2";?> Hour</span></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Payment Method</label>
					<div class="col-md-8">
						<select name="payment" class="form-control">
							<option value="DD">Debit/Normal Online Banking</option>
							<option value="CC">Credit Card</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label">Schedule</label>
					<div class="col-md-8">
						<?php
							$pembagi = floor(count($availableDate['result']) / 3);
							foreach($availableDate['result'] as $index => $row) {
								if($index == 0)
									echo "<div class='col-md-4'>";
								
								echo "<input type='checkbox' name='time[]'";
								if($scheduleId == $row->ID)
									echo " checked='checked'";
								echo " value='{$row->ID}'/> ". date("H:i", strtotime($row->TIME)) . "<br/>";
								
								if(($index+1) % ($pembagi ? $pembagi : 1) == 0) {
									echo "</div>
												<div class='col-md-4'>";
								}
								if($index == (count($availableDate['result']) - 1))
									echo "</div>";
							}
						?>
					</div>
				</div>
				<input type="hidden" name="date" value="<?=$date?>"/>
				<div class="form-group">
					<?php $DDCharge = $charge['eGHL']['DD'] + ($charge['eGHL']['DD']*$charge['eGHL']['PLUS'])/100 + $charge['playHouse']['DD'];?>
					<label class="col-md-4 control-label">Total Payment</label>
					<div class="col-md-8">
						<p class="form-control-static" id="info-book">Total Hour&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="text-center">1 x MYR <?=number_format($court['result']->PRICE,2)?></span>
							<span class="pull-right">MYR <?=number_format($court['result']->PRICE,2)?></span></p>
						<p class="form-control-static" id="info-charge">Credit Charge
							<span class="pull-right">MYR <?=number_format($DDCharge,2)?></span></p>
						<hr/>
						<p class="form-control-static" id="info-total"><strong>Total
							<span class="pull-right">MYR <?=number_format($court['result']->PRICE + $DDCharge, 2)?></span></strong></p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-8 col-md-push-4">
						<button type="submit" class="btn">Book Now!</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	var price = <?=$court['result']->PRICE?>;
	var eGHL_DD = <?=$charge['eGHL']['DD']?>;
	var eGHL_CC = <?=$charge['eGHL']['CC']?>;
	var eGHL_PLUS = <?=$charge['eGHL']['PLUS']?>;
	var playHouse_DD = <?=$charge['playHouse']['DD']?>;
	var playHouse_CC = <?=$charge['playHouse']['CC']?>;
	var charge = eGHL_DD + (eGHL_DD * eGHL_PLUS) / 100 + playHouse_DD;
	var book_price = price;
	
	$("[name=payment]").change(function(){
		var eGHL_charge;
		var playHouse_charge;
		if($(this).val() == "DD") {
			eGHL_charge = eGHL_DD + (eGHL_DD * eGHL_PLUS) / 100;
			playHouse_charge = playHouse_DD;
		}else if($(this).val() == "CC") {
			eGHL_charge = (book_price * eGHL_CC) / 100;
			eGHL_charge = eGHL_charge + (eGHL_charge * eGHL_PLUS) / 100;
			playHouse_charge = playHouse_CC;
		}
		charge = eGHL_charge + playHouse_charge;
		$("#info-charge > span").html("MYR " + charge.toFixed(2));
		$("#info-total span").html("MYR " + (book_price + charge).toFixed(2));
	});
	
	$("[name='time[]']").change(function(){
		var nCourt = $("[name='time[]']:checked").length;
		book_price = nCourt * price;
		
		if($("[name=payment]").val() == "CC") {
			var playHouse_charge = playHouse_CC;
			var eGHL_charge = (book_price * eGHL_CC) / 100;
			eGHL_charge = eGHL_charge + (eGHL_charge * eGHL_PLUS) / 100;
			charge = eGHL_charge + playHouse_charge;
			$("#info-charge > span").html("MYR " + charge.toFixed(2));
		}
		
		$("#info-book > span.text-center").html(nCourt + " x MYR " + price.toFixed(2));
		$("#info-book > span.pull-right").html("MYR " + book_price.toFixed(2));
		$("#info-total span").html("MYR " + (book_price + charge).toFixed(2));
	});
});
</script>