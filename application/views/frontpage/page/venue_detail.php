<section class="section-green">
	<div class="container">
		<div class="row">
			<?php if(!$venue['is_error']): ?>
				<div class="col-md-7">
					<div class="box-detail">
						<h1><?=$venue['result']->NAME;?></h1>
					</div>
					<div class="box-detail">
						<div class="main-photo">
							<div id="slider-carousel" class="carousel horizontal slide" data-ride="carousel">
								<div class="carousel-inner">
									<?php
										// MAIN PHOTO
										$photoIteration = 0;
										foreach($court['result'] as $row) {
											if($row->PHOTO) {
												echo "<div class=\"item";
															if($photoIteration == 0)
																echo " active";
												echo										"\">
																<img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\" class=\"img-responsive\" alt=\"\"/>
																<div class=\"col-md-12 photo-title\">
																	<h3>{$row->NAME}</h3>
																</div>
															</div>";
												
												++ $photoIteration;
											}
										}
										
										if($photoIteration == 0) {
											echo "<div class=\"item active\">
															<img src=\"". base_url("photo/gallery/DEFAULT.jpg") ."\" class=\"img-responsive\" alt=\"\"/>
														</div>";
										}
									?>
									
								</div>
								<?php if($photoIteration > 1): ?>
									<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
					
					<div class="box-detail">
						<div id="map" style="height:200px;width:100%;"></div>
						<script src="https://maps.googleapis.com/maps/api/js"></script>
						<script>
							var map, marker, position = {lat: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LAT : $venue['result']->MAPS_LAT_LOC;?>, lng: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LNG : $venue['result']->MAPS_LNG_LOC;?>};
							function initMap() {
									map = new google.maps.Map(document.getElementById('map'), {
											center: position,
											zoom  : <?=$venue['result']->MAPS_LAT ? 15 : 11?>,
											styles:[
															{
																	"elementType": "geometry",
																	"stylers": [
																			{
																					"hue": "#ff4400"
																			},
																			{
																					"saturation": -68
																			},
																			{
																					"lightness": -4
																			},
																			{
																					"gamma": 0.72
																			}
																	]
															},
															{
																	"featureType": "road",
																	"elementType": "labels.icon"
															},
															{
																	"featureType": "landscape.man_made",
																	"elementType": "geometry",
																	"stylers": [
																			{
																					"hue": "#0077ff"
																			},
																			{
																					"gamma": 3.1
																			}
																	]
															},
															{
																	"featureType": "water",
																	"stylers": [
																			{
																					"hue": "#00ccff"
																			},
																			{
																					"gamma": 0.44
																			},
																			{
																					"saturation": -33
																			}
																	]
															},
															{
																	"featureType": "poi.park",
																	"stylers": [
																			{
																					"hue": "#44ff00"
																			},
																			{
																					"saturation": -23
																			}
																	]
															},
															{
																	"featureType": "water",
																	"elementType": "labels.text.fill",
																	"stylers": [
																			{
																					"hue": "#007fff"
																			},
																			{
																					"gamma": 0.77
																			},
																			{
																					"saturation": 65
																			},
																			{
																					"lightness": 99
																			}
																	]
															},
															{
																	"featureType": "water",
																	"elementType": "labels.text.stroke",
																	"stylers": [
																			{
																					"gamma": 0.11
																			},
																			{
																					"weight": 5.6
																			},
																			{
																					"saturation": 99
																			},
																			{
																					"hue": "#0091ff"
																			},
																			{
																					"lightness": -86
																			}
																	]
															},
															{
																	"featureType": "transit.line",
																	"elementType": "geometry",
																	"stylers": [
																			{
																					"lightness": -48
																			},
																			{
																					"hue": "#ff5e00"
																			},
																			{
																					"gamma": 1.2
																			},
																			{
																					"saturation": -23
																			}
																	]
															},
															{
																	"featureType": "transit",
																	"elementType": "labels.text.stroke",
																	"stylers": [
																			{
																					"saturation": -64
																			},
																			{
																					"hue": "#ff9100"
																			},
																			{
																					"lightness": 16
																			},
																			{
																					"gamma": 0.47
																			},
																			{
																					"weight": 2.7
																			}
																	]
															}
													]
									});
									<?php if($venue['result']->MAPS_LAT) { ?>
									marker = new google.maps.Marker({
											position : position,
											map      : map,
											title    : '<?=$venue['result']->NAME?>'
									});
									<?php } ?>
							}
							
							google.maps.event.addDomListener(window, 'load', initMap);
						</script>
					</div>
					
					<div class="box-detail">
						<div class="col-md-12 gallery-fix">
							<ul class="gallery-box">
								<?php
									// GALLERY
									$i = 0;
									foreach($gallery['result'] as $row) {
										echo "<li class=\"gallery-item";
										if($i == 0)
											echo " first";
										echo "\"><a class=\"preview\" href=\"". base_url("photo/gallery/". $row->PHOTO) ."\" rel=\"prettyPhoto[0]\"><img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\"/></a></li>";
										
										++ $i;
									}
								?>
							</ul>
							<a href="javascript:void(0)" id="gallery-left-click" class="left control-carousel hidden-xs">
								<i class="fa fa-angle-left"></i>
							</a>
							<a href="javascript:void(0)" id="gallery-right-click" class="right control-carousel hidden-xs">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
						<script>
							$(document).ready(function(){
								$("#gallery-right-click").click(function(){
									var width = 0;
									
									$('.gallery-box > li.gallery-item').each(function() {
											width += $(this).outerWidth( true );
									});
									
									var left = $(".gallery-box").position().left;
									var offset = width - ($(".gallery-fix").width() - left);
									
									if(offset > 300) {
										$(".gallery-box").css({
											"left": (left - 300)
										});
									}
									else if(offset > 0) {
										$(".gallery-box").css({
											"left": (left - offset)
										});
									}
								});
								
								$("#gallery-left-click").click(function(){
									var left = $(".gallery-box").position().left;
									
									if(left < -300) {
										$(".gallery-box").css({
											"left": (left + 300)
										});
									}
									else if(left < 0) {
										$(".gallery-box").css({
											"left": (left - left)
										});
									}
								});
							});
						</script>
						<script>
							jQuery(function($) {'use strict',	
								//Pretty Photo
								$("a[rel^='prettyPhoto']").prettyPhoto({
									social_tools: false,
									theme: 'dark_rounded',
									slideshow: 5000
								});	
							});
						</script>
					</div>
				</div>
				<div class="col-md-5 right-side">
					<div class="box-detail col-md-12">
						<h4>About <?=$venue['result']->NAME?> | <?=$type['result']->TYPE?></h4>
						<p><?=$venue['result']->DESCRIPTION?></p>
					</div>
					
					<?php
						$av = $av ? $av : date("d/m/Y");
						$avParse = explode("/", $av);
					?>
					
					<div class="box-detail col-md-12">
						<ul class="book-list">
							<?php foreach($availableStrict['result'] as $index => $row) : ?>
							<?php
								$day = date("D", strtotime($date));
								$schedule = $this->Venuemodel->get_schedule_detail($venue['result']->VENUE_ID, $row->COURT_NAME, "$from:00", $day);
								if(!$schedule['is_error']) :
							?>
							<li class="col-md-12 <?php if($index == (count($availableStrict['result'])-1) && $availableStrict2['is_error']) echo "last";?>">
								<div class="col-md-8">
									<div class="col-md-6">
										<span class="col-md-12"><?=$row->COURT_NAME?></span>
										<span class="col-md-12">RM <?=number_format($row->PRICE, 0, ".", ",")?>,-</span>
									</div>
									<div class="col-md-6">
										<span class="col-md-12"><?=date("F dS", strtotime($date))?></span>
										<span class="col-md-12"><?=date("H:i", strtotime($schedule['result']->TIME))?></span>
									</div>
								</div>
								<div class="col-md-4">
									<a href="<?=site_url("page/start_book/{$schedule['result']->SCHEDULE_ID}/$date")?>" class="btn"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;BOOK NOW</a>
								</div>
							</li>
							<?php endif; ?>
							<?php endforeach; ?>
							
							<?php if(!$availableStrict2['is_error']): ?>
							<?php foreach($availableStrict2['result'] as $index => $row) : ?>
							<li class="col-md-12 <?php if($index == (count($availableStrict2['result'])-1)) echo "last";?>">
								<div class="col-md-8">
									<div class="col-md-6">
										<span class="col-md-12"><?=$row->COURT_NAME?></span>
										<span class="col-md-12">RM <?=number_format($row->PRICE, 0, ".", ",")?>,-</span>
									</div>
									<div class="col-md-6">
										<?php
											$day = date("D", strtotime("$avParse[2]-$avParse[1]-$avParse[0]"));
											$schedule = $this->Venuemodel->get_schedule_detail($venue['result']->VENUE_ID, $row->COURT_NAME, "$from:00", $day);
										?>
										<span class="col-md-12"><?=date("F dS", strtotime("$avParse[2]-$avParse[1]-$avParse[0]"))?></span>
										<span class="col-md-12"><?=date("H:i", strtotime($schedule['result']->TIME))?></span>
									</div>
								</div>
								<div class="col-md-4">
									<a href="<?=site_url("page/start_book/{$schedule['result']->SCHEDULE_ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>" class="btn"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;BOOK NOW</a>
								</div>
							</li>
							<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
					
					<div class="box-detail col-md-12">
						<div class="form-group" style="padding:12px 0 35px;">
							<label class="col-md-3 control-label" style="text-align:left">Available Date</label>
							<div class="col-md-6">
								<?php
									$dateParse = explode("-", $date);
								?>
								<form action="" method="GET" id="form-av">
									<input type="hidden" name="d" value="<?="$dateParse[2]/$dateParse[1]/$dateParse[0]"?>"/>
									<input type="hidden" name="rt" value="<?="$from,$to"?>"/>
									<div class="input-group date" id="datepicker1">
										<input type="text" name="av" class="form-control" onChange="set_av()" placeholder="dd/mm/yyyy" value="<?=$av ? $av : date("d/m/Y")?>" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</form>
							</div>
							<div class="col-md-3">
								<button type="submit" class="btn" form="form-av"><i class="fa fa-search"></i> Check</button>
							</div>
						</div>
						
						<table class="table">
							<thead>
								<tr>
									<th>Court</th>
									<th style="width:23%">Price</th>
									<th style="width:50%">Available Time</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$courtName = "";
								foreach($availableDate['result'] as $index => $row){
									if($row->COURT_NAME != $courtName) {
										if($index != 0) {
											echo "	</td>
														</tr>";
										}
								?>
									<tr>
										<td><?=$row->COURT_NAME?></td>
										<td>RM <?=number_format($row->PRICE, 0, ".", ",")?>,-</td>
										<td><a data-toggle="tooltip" title="Book This" href="<?=site_url("page/start_book/{$row->ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>"><?=date("H:i", strtotime($row->TIME))?></a>
								<?php
										$courtName = $row->COURT_NAME;
									}
									else {
										?>| <a data-toggle="tooltip" title="Book This" href="<?=site_url("page/start_book/{$row->ID}/$avParse[2]-$avParse[1]-$avParse[0]")?>"><?=date("H:i", strtotime($row->TIME))?></a> <?php
									}
									
									if($index == (count($availableDate['result']) - 1)) {
										echo "	</td>
													</tr>";
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
		
			<?php else: ?>
				<div class="col-md-12">
					<div class="box-detail">
						<h2>Venue Not Found!</h2>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>