<div role="main" class="ui-content">
	<form action="<?=site_url("page/m/search_result")?>" method="GET" class="form-horizontal">
		<label for="form-search-location" class="select">Location</label>
		<select name="loc" id="form-search-location" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_location", array("location" => $location))
			?>
		</select>
		
		<label for="form-search-type" class="select">Type</label>
		<select name="tp" id="form-search-type" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_type", array("type" => $type))
			?>
		</select>
		
		<label for="form-search-venue" class="select">Venue</label>
		<select name="ven" id="form-search-venue" data-mini="true">
			<?php
				$this->load->view("frontpage/ajax/form_venue", array("venue" => $venue))
			?>
		</select>
		<script>
			$("#form-search-location, #form-search-type").change(function () {
				$.ajax({
					type:'GET',
					url:'<?=site_url('page/form_get_venue')?>',
					data:{
								'location':$("#form-search-location").val(), 
								'type':$("#form-search-type").val()
							 },
					success:function(data){
						$("#form-search-venue").html(data);
					},
					error:function(data){
						$("#form-search-venue").html("<optgroup label=\"Error!\"></optgroup>");
					}
				});
			});
		</script>
		
		<label for="form-search-date" class="">Book Date</label>
		<input type="date" name="d" id="form-search-date" value="" />
		
		<div data-role="rangeslider" data-mini="true">
			<label for="range-2a" class="">Available In</label>
			<input type="range" name="rt-from" id="range-2a" min="0" max="24" value="13">
			<label for="range-2b" class="">Available In</label>
			<input type="range" name="rt-to" id="range-2b" min="0" max="24" value="15">
    </div>
		
		<button type="submit" class="ui-btn ui-mini ui-icon-search ui-btn-icon-left">Find It</button>
	</form>
</div><!-- /content -->	