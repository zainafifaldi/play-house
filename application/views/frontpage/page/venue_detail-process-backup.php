<?php if(!$venue['is_error']): ?>
<h3><?=$venue['result']->NAME;?></h3>
<?php
	// MAIN PHOTO
	$photoIteration = 0;
	foreach($court['result'] as $row) {
		if($row->PHOTO) {
			echo "<img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\" height=\"300\"/>";
			echo $row->NAME;
			++ $photoIteration;
		}
	}
	
	if($photoIteration == 0)
		echo "<img src=\"". base_url("photo/gallery/DEFAULT.jpg") ."\" height=\"300\"/>";
?>
<br/>
<?php
	// GALLERY
	foreach($gallery['result'] as $row) {
		echo "<img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\" height=\"100\"/>";
	}
?>
<br/>
<div id="map" style="height:400px;width:600px;"></div>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
	var map, marker, position = {lat: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LAT : -6.8722711;?>, lng: <?=$venue['result']->MAPS_LAT ? $venue['result']->MAPS_LNG : 107.5919395;?>};
	function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
					center: position,
					zoom  : 15
			});
			marker = new google.maps.Marker({
					position : position,
					map      : map,
					title    : '<?=$venue['result']->NAME?>'
			});
	}
	google.maps.event.addDomListener(window, 'load', initMap);
</script>
<?php else: ?>
Venue NOT Found!
<?php endif; ?>