<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vendor
			<small>Add New</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-12">
				<div class="box box-primary">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Data Venue</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_add")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_add")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("admin/add_vendor_proc")?>" method="post" id="form-add" class="">
							<div class="form-group">
								<label for="input-name">Venue Name*</label>
								<input type="text" name="name" class="form-control" id="input-name" value="<?=$this->session->flashdata("name");?>" required/>
							</div>
							<div class="form-group">
								<label for="input-address">Address*</label>
								<textarea name="address" class="form-control" id="input-address" required/><?=$this->session->flashdata("address");?></textarea>
							</div>
							<div class="form-group">
								<label for="input-location">Location of Venue*</label>
								<select name="loc" class="form-control" id="input-location" required onChange="loc_change()">
									<?php
										$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $this->session->flashdata("loc"), "new" => true))
									?>
								</select>
							</div>
							<div id="loc-collapse" style="display:none;">
								<div class="form-group">
									<label for="input-country">Country</label>
									<input type="text" name="country" class="form-control" id="input-country" value="<?=$this->session->flashdata("country");?>" disabled/>
								</div>
								<div class="form-group">
									<label for="input-city">City</label>
									<input type="text" name="city" class="form-control" id="input-city" value="<?=$this->session->flashdata("city");?>" disabled/>
								</div>
								<div class="form-group">
									<label for="input-lat-long">Latitude, Longitude</label>
									<input type="text" name="lat_long" class="form-control" id="input-lat-long" value="<?=$this->session->flashdata("lat_long");?>" placeholder="LATITUDE, LONGITUDE" disabled/>
								</div>
							</div>
							<script>
								$(document).ready(function(){
									$("[name=loc]").change(function(){
										if($(this).val() == "new") {
											$("#input-country, #input-city, #input-lat-long").removeAttr("disabled");
											$("#input-country, #input-city, #input-lat-long").attr("required", "required");
											$("#loc-collapse").css({
												"display" : "inline"
											});
										}
										else {
											$("#loc-collapse").css({
												"display" : "none"
											});
											$("#input-country, #input-city, #input-lat-long").removeAttr("required");
											$("#input-country, #input-city, #input-lat-long").attr("disabled", "disabled");
										}
									});
								});
							</script>
							<hr/>
							<div class="form-group">
								<label for="input-vendorname">User Vendor Name*</label>
								<input type="text" name="vendor_name" class="form-control" id="input-vendorname" value="<?=$this->session->flashdata("vendor_name");?>" required/>
							</div>
							<div class="form-group">
								<label for="input-username">User Vendor Username*</label>
								<input type="text" name="vendor_username" class="form-control" id="input-username" value="<?=$this->session->flashdata("vendor_username");?>" required/>
							</div>
							<div class="form-group">
								<label for="input-password">User Vendor Password*</label>
								<input type="password" name="vendor_pass" class="form-control" id="input-password" required/>
							</div>
							<div class="form-group">
								<label for="input-repassword">Confirm Password*</label>
								<input type="password" name="vendor_repass" class="form-control" id="input-repassword" required/>
							</div>
							<button type="submit" class="btn btn-primary btn-block">Add Venue</button>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Left col -->
			
		</div>
	</section>
</div>