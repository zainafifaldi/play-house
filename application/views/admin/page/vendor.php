<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Vendor
			<small>Editor</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-12">
				<div class="box box-danger">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Vendor List</h3>
						<div class="box-tools pull-right">
							<a href="<?=site_url("admin/add_vendor")?>" class="text-danger"><i class="fa fa-plus-circle"></i> Add New</a>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_add")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_add")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_add")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_add")?>
							</div>
						<?php } ?>
						
						<table class="table table-bordered table-hover table-dataTable">
							<thead>
								<tr>
									<td>No</td>
									<td>Venue</td>
									<td>Country</td>
									<td>City</td>
									<td>Address</td>
								</tr>
							</thead>
							
							<tbody>
								<?php foreach($venue['result'] as $index => $row): ?>
									<tr>
										<td><?=($index+1)?></td>
										<td><?=$row->NAME?></td>
										<td><?=$row->COUNTRY?></td>
										<td><?=$row->CITY?></td>
										<td><?=$row->ADDRESS?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Left col -->
			
		</div>
	</section>
</div>