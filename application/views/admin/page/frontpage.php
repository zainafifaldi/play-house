<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Frontpage
			<small>Ads Editor</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-5">
				<div class="box box-primary">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Add New</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-add" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_add")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_add")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_add")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_add")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("admin/add_ads")?>" method="post" id="form-add">
							<div class="form-group">
								<label for="input-headline">Headline*</label>
								<input type="text" name="headline" class="form-control" id="input-headline" value="<?=$this->session->flashdata("headline");?>" required/>
							</div>
							<div class="form-group">
								<label for="input-link">Link*</label>
								<input type="text" name="link" class="form-control" id="input-link" value="<?=$this->session->flashdata("link");?>" required/>
							</div>
							<div class="form-group">
								<label for="input-content">Content*</label>
								<textarea name="content" class="form-control" id="input-content" required/><?=$this->session->flashdata("content");?></textarea>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Left col -->
			
			<!-- Right col -->
			<section class="col-lg-7">
				<div class="box box-success">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Advertisement List</h3>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_list")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_list")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_list")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_list")?>
							</div>
						<?php } ?>
						
						<table class="table">
							<thead>
								<tr>
									<th>Headline</th>
									<th>Content</th>
									<th>Link</th>
									<th></th>
								</tr>
							</thead>
							
							<tbody>
								<?php foreach($ads['result'] as $row): ?>
									<tr>
										<td style="color:#00A65A;"><strong><?=$row->HEADLINE?></strong></td>
										<td><?=$row->CONTENT?></td>
										<td><a href="<?=$row->LINK?>" target="_blank"><?=$row->LINK?></a></td>
										<td><a href="javascript:confirm('Are you sure want to close this ads?') ? window.location = '<?=site_url("admin/close_ads/". $row->ADS_ID)?>' : void(0)" style="color:#AA0000;" data-toggle="tooltip" title="Close Ads"><i class="fa fa-close"></i></a></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Right col -->
		</div>
	</section>
</div>