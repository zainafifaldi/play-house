<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<section class="col-md-12">
				<!-- Custom tabs (Charts with tabs)-->
				<div class="box box-primary">
					<?php if($this->session->flashdata("set_done_error")) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_error")?>
						</div>
					<?php } ?>
					<?php if($this->session->flashdata("set_done_success")) { ?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_success")?>
						</div>
					<?php } ?>
					
					<div class="box-body">
						<table class="table table-striped data-table">
							<thead>
								<tr>
									<th>No</th>
									<th>Reservation ID</th>
									<th>Venue</th>
									<th>Type</th>
									<th>Court</th>
									<th>Date</th>
									<th style="width:180px">Schedule</th>
									<th style="text-align:right">Price</th>
									<th>State</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($order_list['result'] as $index => $row): ?>
									<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
									<tr>
										<td style="text-align:right;"><?=($index+1)?></td>
										<td><?=$row->RESERVATION?></td>
										<td><?=$row->NAME?></td>
										<td><?=$row->TYPE_NAME?></td>
										<td><?=$row->COURT?></td>
										<td><?=date("d/m/Y", strtotime($row->BOOK_DATE))?></td>
										<td><?php
												foreach($schedule['result'] as $index => $time) {
													if($index != 0)
														echo " | ";
													
													echo date("H:i", strtotime($time->TIME));
												}
												?></td>
										<td align="right">MYR <?=number_format($row->BOOK_PRICE,2)?></td>
										<td><?php if($row->LOCK_STATE == 2) echo "<strong class='text-green'>Done</strong>";
															else if($row->LOCK_STATE == 1) echo "<strong class='text-blue'>Pending</strong>";?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div><!-- /.nav-tabs-custom -->

			</section><!-- /.Left col -->
		</div>
	</section>
</div>