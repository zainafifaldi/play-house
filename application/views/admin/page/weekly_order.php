<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<section class="col-md-12">
				<!-- Custom tabs (Charts with tabs)-->
				<div class="box box-primary">
					<?php if($this->session->flashdata("set_done_error")) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_error")?>
						</div>
					<?php } ?>
					<?php if($this->session->flashdata("set_done_success")) { ?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_success")?>
						</div>
					<?php } ?>
					
					<div class="box-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>No</th>
									<th>Venue</th>
									<th style="text-align:center">Total Book</th>
									<th style="text-align:right">Total Price</th>
									<th style="text-align:right">Total eGHL</th>
									<th style="text-align:right">Total PH</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($order_list['result'] as $index => $row): ?>
									<tr>
										<td style="text-align:right;"><?=($index+1)?></td>
										<td><a href="<?=site_url("admin/weekly_detail/". $row->VENUE_ID ."/". $offset)?>"><?=$row->NAME?></a></td>
										<td align="center"><?=number_format($row->TOTAL_BOOK,0)?></td>
										<td align="right">MYR <?=number_format($row->TOTAL_PRICE,2)?></td>
										<td align="right">MYR <?=number_format($row->TOTAL_eGHL,2)?></td>
										<td align="right">MYR <?=number_format($row->TOTAL_PH,2)?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<div class="box-footer">
						<div class="box-tools">
							<ul class="pagination pagination-sm no-margin pull-right">
								<li <?php if($offset == 2) echo "class='active'"?>><a href="<?=site_url("admin/weekly_order/2")?>">Last Last Week</a></li>
								<li <?php if($offset == 1) echo "class='active'"?>><a href="<?=site_url("admin/weekly_order/1")?>">Last Week</a></li>
								<li <?php if($offset == 0) echo "class='active'"?>><a href="<?=site_url("admin/weekly_order")?>">This Week</a></li>
							</ul>
						</div>
					</div>
				</div><!-- /.nav-tabs-custom -->

			</section><!-- /.Left col -->
		</div>
	</section>
</div>