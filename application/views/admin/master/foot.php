    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap -->
    <script src="<?=base_url("assets/js/bootstrap.min.js")?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url("assets/js/app.min.js")?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url("assets/js/demo.js")?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")?>"></script>
		<!-- Price Range -->
		<script src="<?=base_url("assets/js/price-range.js")?>"></script>
		<!-- DataTables -->
    <script src="<?=base_url("assets/plugins/datatables/jquery.dataTables.min.js")?>"></script>
    <script src="<?=base_url("assets/plugins/datatables/dataTables.bootstrap.min.js")?>"></script>
    <script>
			$("#range-time").slider({
				id: "range-slider",
				min: 7,
				max: 24,
				range: true,
				step: 1,
				value: [8, 22],
				tooltip_placement: "bottom"
			});
		</script>
		<script>
      $(function () {
        $(".table-dataTable").DataTable();
        $('.table-dataTable2').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
		<script>
      $(function () {
        $(".data-table").DataTable();
      });
    </script>
  </body>
</html>