<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">APH</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Admin <b>Play</b>House</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
						<li class="treeview<?php if($active=="all_order" || $active=="weekly_order") echo ' active';?>">
              <a href="#">
                <i class="fa fa-list-alt"></i> <span>Order</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
								<li <?php if($active=="all_order") echo 'class="active"';?>><a href="<?=site_url("admin/all_order")?>"><i class="fa fa-circle-o"></i> All Order</a></li>
								<li <?php if($active=="weekly_order") echo 'class="active"';?>><a href="<?=site_url("admin/weekly_order")?>"><i class="fa fa-circle-o"></i> Weekly Order</a></li>
              </ul>
            </li>
						<li <?php if($active=="frontpage") echo 'class="active"';?>>
              <a href="<?=site_url("admin/frontpage")?>">
                <i class="fa fa-home"></i> <span>Frontpage</span>
              </a>
            </li>
            <li <?php if($active=="vendor") echo 'class="active"';?>>
              <a href="<?=site_url("admin/vendor")?>">
                <i class="fa fa-briefcase"></i> <span>Vendor</span>
              </a>
            </li>
            <li>
              <a href="<?=site_url("admin/logout")?>">
                <i class="fa fa-sign-out"></i> <span>Logout</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>