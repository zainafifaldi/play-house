<div class="modal fade" id="policy-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Privacy Policy</h4>
			</div>
			<div class="modal-body">
				<fieldset class="terms-field">
					<h1 class="terms-title">PRIVACY POLICY</h1>
					
					<ol class="list-number">
						<li>
							<span class="list-title">Collection of Personal Data</span>
							<p>
								This Personal Data Protection Notice is issued to all our valued customers/prospective customers, pursuant to the requirements of the Personal Data Protection Act 2010. 
								We treat and view your personal data seriously. 
								In the course of your dealings with us, Finebyte Sdn Bhd and its related subsidiaries (as defined under the Companies Act 1965) ("Finebyte"), as our valued customer / prospective customer, we will request that you provide data and information about yourself ("Personal Data") to enable us to enter into transaction with you or to deliver the necessary notices, services and/or products.
							</p>
						</li>
						
						<li>
							<span class="list-title">Nature of Personal Data</span>
							<p>
								Such Personal Data may be subject to applicable data protection, privacy and other similar laws and may include information concerning name, age, identity card number, passport number, address, gender, date of birth, marital status, occupation, income range, contact information, email address, race, ethnic origin, nationality and credit card details, next of kin, employer, bankers.
							</p>
						</li>
						
						<li>
							<span class="list-title">Impact from failure to supply Personal Data</span>
							<p>
								The failure to supply such Personal Data will:-
							</p>
							<ol class="list-alphabet">
								<li>Result in us being unable to provide you with the notices, bookings, services and/or products requested;</li>
								<li>Result in us being unable to accept and process your entry to any contest;</li>
								<li>Result in us being unable to update you on our latest products, services and promotions.</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">Purpose of Collecting Personal Data</span>
							<p>
								The Personal Data is collected, used and otherwise processed by us for, amongst others, the following purposes:
							</p>
							<ol class="list-alphabet">
								<li>Processing your request(s), order(s), booking(s), purchase(s) and/or contest entry;</li>
								<li>Delivering notices, services, products, updates, booking confirmations, prizes and/or promotional materials to you;</li>
								<li>Maintaining and improving customer relationship;</li>
								<li>Conducting marketing survey and client profiling activities;</li>
								<li>Maintaining and updating internal record keeping;</li>
								<li>Detecting and preventing crime (including but not limited to fraud, money-laundering, bribery);</li>
								<li>Meeting any legal or regulatory requirements and making disclosure under the requirements of any applicable law, regulation, direction, court order, by-law, guideline, circular, code applicable to us or any entity within our group;</li>
								<li>Enabling us to send you information by email, telecommunication means (telephone calls or text messages) or social media about products, services and/or promotions offered by Finebyte and selected third parties that we think may interest you;</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">Disclosure</span>
							<p>
								The Personal Data provided to us will generally be kept confidential but you hereby consent and authorize us to provide or disclose your Personal Data to the following categories:-
							</p>
							<ol class="list-alphabet">
								<li>Any person to whom we are compelled or required to do so under law ;</li>
								<li>Any entity within our company and subsidiaries;</li>
								<li>Payment channels including without limitation financial institutions for purpose of assessing, verifying, effectuating and facilitating payment of any amount due to Finebyte in connection with your acquisition of our services, bookings, and/or products;</li>
								<li>Our business partners and affiliates that provide related services or products in connection with our business, for joint promotions and/or contest;</li>
								<li>Statutory authorities, government agencies and industry regulators;</li>
								<li>Our consultants, accountants, auditors, lawyers or other financial or professional advisers;</li>
								<li>Our sub-contractors or third party service or product providers;</li>
								<li>Our service providers for purposes of establishing and maintaining a common database where we have a legitimate common interest;</li>
								<li>The general public when you become a winner in a contest by publishing your name, photographs and other Personal Data without compensation for advertising and publicity purposes.</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">Safeguards</span>
							<p>
								We shall keep and process your data in a secure manner. We endeavour, where practicable, to implement the appropriate administrative and security safeguards and procedures in accordance with the applicable laws and regulations to prevent the unauthorized or unlawful processing of the Personal Data and the accidental loss or destruction of, or damage to, the Personal Data.
							</p>
						</li>
						
						<li>
							<span class="list-title">Rights of Access and Correction</span>
							<ol class="list-sub-number">
								<li>You have the right to request for access to and correction of your information held by us and in this respect, you may:
									<ol class="list-alphabet">
										<li>Check whether we hold or use your Personal Data and request access to such data;</li>
										<li>Request that we correct any of your Personal Data that is inaccurate, incomplete or out-of-date;</li>
										<li>Request that your Personal Data is retained by us only as long as necessary for the fulfilment of the purposes for which it was collected;</li>
										<li>Request that we specify or explain our policies and procedures in relation to data and types of Personal Data handled by us;</li>
										<li>Communicate to us your objection to the use of your Personal Data for marketing purposes whereupon we will not use your Personal Data for these purposes; and</li>
										<li>Withdraw, in full or in part, your consent given previously, in each case subject to any applicable legal restrictions, contractual conditions and a reasonable time period.</li>
									</ol>
								</li>
								<li>
									The department to whom written requests for access to the Personal Data or correction and/or deletion of the Personal Data or for information regarding policies and procedures and types of Personal Data handled by us is:<br/><br/>
									Department Name: Customer Service<br/>
									Contact No: 012 215 7233<br/>
									Email Address: contact.us@playhouseapps.com<br/>
									Address: 36-1, Jalan Mawar 17, Dataran Mestika, Taman Mestika 56100 Kuala Lumpur.<br/>
								</li>
								<li>
									You may also unsubscribe to our marketing materials by clicking the unsubscribe link contained in the email we send to you and following the instructions therein.
								</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">Consent</span>
							<p>
								Your usage of our website indicates your consent to the use of your Personal Data in accordance with this Personal Data Protection Notice.
							</p>
						</li>
					</ol>
					
					<p>
						When you perform any e-Payment transaction from our site, your computer automatically accesses a small piece of code on our server (a 'server-side cookie') which enables the web pages to be displayed correctly. This cookie is not saved on your computer and is only used for the duration of the session.
					</p>
					<p>
						If you are using a web browser set to high security you may experience difficulties using this facility.
					</p>
				</fieldset>
			</div>
		  <div class="modal-footer" style="margin-top:0">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</div>
</div>