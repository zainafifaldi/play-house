<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">VPH</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Vendor <b>Play</b>House</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
						<li <?php if($active=="order") echo 'class="active"';?>>
              <a href="<?=site_url("vendor/order")?>">
                <i class="fa fa-list-alt"></i> <span>Order</span>
              </a>
            </li>
            <li <?php if($active=="venue") echo 'class="active"';?>>
              <a href="<?=site_url("vendor/venue")?>">
                <i class="fa fa-building"></i> <span>Venue</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-map-marker"></i> <span>Court</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
								<?php foreach($court_list['result'] as $row) { ?>
                <li <?php if($active=="court") echo 'class="active"';?>><a href="<?=site_url("vendor/court/{$row->NAME}")?>">
								<img src="<?php
														if($row->TYPE == "1")
															echo base_url("assets/images/assets/PH_ico_football.png");
														else if($row->TYPE == "2")
															echo base_url("assets/images/assets/PH_ico_batminton.png");
														else if($row->TYPE == "3")
															echo base_url("assets/images/assets/PH_ico_tennis.png");
														else if($row->TYPE == "4")
															echo base_url("assets/images/assets/PH_ico_bowling.png");
													?>" alt="" height="15px"/>&nbsp;&nbsp;
								<?=$row->NAME?></a></li>
								<?php } ?>
                <li <?php if($active=="add_court") echo 'class="active"';?>><a href="<?=site_url("vendor/add_court")?>"><i class="fa fa-plus"></i> Add Court</a></li>
              </ul>
            </li>
						<li>
              <a href="<?=site_url("vendor/logout")?>">
                <i class="fa fa-sign-out"></i> <span>Logout</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>