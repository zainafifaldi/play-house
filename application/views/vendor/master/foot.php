    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url("assets/js/jquery.js")?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap -->
    <script src="<?=base_url("assets/js/bootstrap.min.js")?>"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url("assets/js/app.min.js")?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?=base_url("assets/js/pages/dashboard.js")?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=base_url("assets/js/demo.js")?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url("assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js")?>"></script>
		<!-- Price Range -->
		<script src="<?=base_url("assets/js/price-range.js")?>"></script>
		<script>
			$("#range-time").slider({
				id: "range-slider",
				min: 7,
				max: 24,
				range: true,
				step: 1,
				value: [8, 22],
				tooltip_placement: "bottom"
			});
		</script>
  </body>
</html>