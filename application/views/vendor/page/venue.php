<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Venue
			<small>Editor</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<section class="col-lg-7">
				<div class="box box-primary">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Description</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-desc" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_desc")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_desc")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_desc")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_desc")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("vendor/save_ven_desc")?>" method="post" id="form-desc">
							<div>
								<textarea name="desc" class="textarea" placeholder="Type Description Here..." style="width: 100%; height: 275px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$venue['result']->DESCRIPTION;?></textarea>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Left col -->
			
			<!-- Right col -->
			<section class="col-lg-5">
				<div class="box box-success">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Detail</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-detail" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_detail")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_detail")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_detail")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_detail")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("vendor/save_ven_detail")?>" method="post" id="form-detail">
							<div class="form-group">
								<label for="input-name">Venue Name*</label>
								<input type="text" name="name" class="form-control" id="input-name" value="<?=$venue['result']->NAME;?>" required/>
							</div>
							<div class="form-group">
								<label for="input-address">Address*</label>
								<textarea name="address" class="form-control" id="input-address" required/><?=$venue['result']->ADDRESS;?></textarea>
							</div>
							<hr/>
							<div class="form-group">
								<label for="input-location">Location of Venue*</label>
								<select name="loc" class="form-control" id="input-location" required>
									<?php
										$this->load->view("frontpage/ajax/form_location", array("location" => $location, "loc" => $venue['result']->LOCATION_ID))
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="input-latitude">LATITUDE - Maps Position</label>
								<input type="number" name="lat" class="form-control" id="input-latitude" value="<?=$venue['result']->MAPS_LAT;?>" step="any"/>
							</div>
							<div class="form-group">
								<label for="input-longitude">LONGITUDE - Maps Position</label>
								<input type="number" name="lng" class="form-control" id="input-longitude" value="<?=$venue['result']->MAPS_LNG;?>" step="any"/>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Right col -->
		</div>
	</section>
</div>