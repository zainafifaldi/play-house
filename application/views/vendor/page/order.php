<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Order
			<small>Your venue order detail</small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
			<section class="col-md-12">
				<!-- Custom tabs (Charts with tabs)-->
				<div class="nav-tabs-custom">
					<!-- Tabs within a box -->
					<ul class="nav nav-tabs pull-right">
						<li><a href="#history-order" data-toggle="tab">History</a></li>
						<li><a href="#pending-order" data-toggle="tab">Pending</a></li>
						<li class="active"><a href="#success-order" data-toggle="tab">Success</a></li>
						<li class="pull-left header"><i class="ion ion-clipboard"></i> Table Order</li>
					</ul>
					
					<?php if($this->session->flashdata("set_done_error")) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_error")?>
						</div>
					<?php } ?>
					<?php if($this->session->flashdata("set_done_success")) { ?>
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
							<?=$this->session->flashdata("set_done_success")?>
						</div>
					<?php } ?>
					
					<div class="tab-content no-padding">
						<div class="tab-pane active" id="success-order" style="position: relative; height: 300px;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Reservation ID</th>
										<th>Type</th>
										<th>Court</th>
										<th>Date</th>
										<th style="width:180px">Schedule</th>
										<th style="text-align:right">Price</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($success_order_list['result'] as $index => $row): ?>
										<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
										<tr>
											<td style="text-align:right;"><?=($index+1)?></td>
											<td><?=$row->RESERVATION?></td>
											<td><?=$row->TYPE_NAME?></td>
											<td><?=$row->COURT?></td>
											<td><?=date("d/m/Y", strtotime($row->BOOK_DATE))?></td>
											<td><?php
													foreach($schedule['result'] as $index => $time) {
														if($index != 0)
															echo " | ";
														
														echo date("H:i", strtotime($time->TIME));
													}
													?></td>
											<td align="right">MYR <?=number_format($row->BOOK_PRICE,2)?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane" id="pending-order" style="position: relative; height: 300px;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Reservation ID</th>
										<th>Type</th>
										<th>Court</th>
										<th>Date</th>
										<th style="width:180px">Schedule</th>
										<th style="text-align:right">Price</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($pending_order_list['result'] as $index => $row): ?>
										<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
										<tr>
											<td style="text-align:right;"><?=($index+1)?></td>
											<td><?=$row->RESERVATION?></td>
											<td><?=$row->TYPE_NAME?></td>
											<td><?=$row->COURT?></td>
											<td><?=date("d/m/Y", strtotime($row->BOOK_DATE))?></td>
											<td><?php
													foreach($schedule['result'] as $index => $time) {
														if($index != 0)
															echo " | ";
														
														echo date("H:i", strtotime($time->TIME));
													}
													?></td>
											<td align="right">MYR <?=number_format($row->BOOK_PRICE,2)?></td>
											<td class="text-muted">On <?=date("d/m/Y H:i:s", strtotime($row->BOOK_TIMESTAMP))?></td>
											<td><a href="javascript:confirm('Are you sure want to set this order to done?') ? window.location='<?=site_url("vendor/set_done/{$row->RESERVATION}")?>' : void(0)" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Set Done</a></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						
						<div class="tab-pane" id="history-order" style="position: relative;">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Reservation ID</th>
										<th>Type</th>
										<th>Court</th>
										<th>Date</th>
										<th style="width:180px">Schedule</th>
										<th style="text-align:right">Price</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($history_order_list['result'] as $index => $row): ?>
										<?php $schedule = $this->Bookmodel->get_schedule_by_reservation($row->RESERVATION);?>
										<tr>
											<td style="text-align:right;"><?=($index+1)?></td>
											<td><?=$row->RESERVATION?></td>
											<td><?=$row->TYPE_NAME?></td>
											<td><?=$row->COURT?></td>
											<td><?=date("d/m/Y", strtotime($row->BOOK_DATE))?></td>
											<td><?php
													foreach($schedule['result'] as $index => $time) {
														if($index != 0)
															echo " | ";
														
														echo date("H:i", strtotime($time->TIME));
													}
													?></td>
											<td align="right">MYR <?=number_format($row->BOOK_PRICE,2)?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- /.nav-tabs-custom -->

			</section><!-- /.Left col -->
		</div>
	</section>
</div>