<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Court
			<small><?=$edit_type;?></small>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		<!-- Main row -->
		<div class="row">
		<?php if(isset($detail)): ?>
			<!-- Left col -->
			<section class="col-lg-7">
				<div class="box box-primary">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Gallery</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-desc" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_gallery")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_gallery")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_gallery")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_gallery")?>
							</div>
						<?php } ?>
						
						<form action="<?=site_url("vendor/save_court_gallery")?>" method="post" id="form-desc" enctype="multipart/form-data">
							<div class="form-group col-md-12">
								<input type="hidden" name="court" value="<?=$detail['result']->NAME?>"/>
								<input type="file" name="photo[]" class="form-control" multiple="multiple" accept="image/jpeg"/>
							</div>
						</form>
						
						<?php if($detail['result']->PHOTO) { ?>
						<div class="col-md-12">
							<img src="<?=base_url("photo/gallery/". $detail['result']->PHOTO)?>" class="img-responsive"/>
						</div>
						<?php } ?>
						
						<div class="col-md-6">
							<?php
								$i = 0;
								$center = ceil(count($gallery['result']) / 2);
								foreach($gallery['result'] as $row) {
									if($i == $center) {
										echo "</div>";
										echo "<div class=\"col-md-6\">";
									}
									
									echo "<div class=\"col-md-12 each-gallery-box\" style=\"padding:0\">
													<img src=\"". base_url("photo/gallery/". $row->PHOTO) ."\" class=\"img-responsive\"/>
													<div class=\"wrapper\">
														<div class=\"mask-action\">
															<div class=\"image-action\">
																<a href=\"". site_url("vendor/set_main_photo/". $detail['result']->NAME ."/". $row->GALLERY_ID) ."\" class=\"btn btn-danger\">Make Main Photo</a>
																<a href=\"javascript:confirm('Are you sure want to delete this photo?') ? window.location = '". site_url("vendor/delete_photo/". $detail['result']->NAME ."/". $row->GALLERY_ID) ."' : void(0)\" class=\"btn btn-danger\">Delete</a>
															</div>
														</div>
													</div>
												</div>";
									
									++ $i;
								}
							?>
						</div>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Left col -->
		<?php endif; ?>
			
			<!-- Right col -->
			<section class="<?=isset($detail) ? "col-lg-5" : "col-lg-12"?>">
				<div class="box box-success">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Detail</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-detail" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_detail")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_detail")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_detail")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_detail")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("vendor/save_court_detail")?>" method="post" id="form-detail">
							<input type="hidden" name="act" value="<?=isset($detail) ? "2" : "1"?>"/>
							<div class="form-group">
								<label for="input-name">Court Name*</label>
								<input type="text" name="name" class="form-control" id="input-name" value="<?=isset($detail) ? $detail['result']->NAME : ""?>" required/>
								<?=isset($detail) ? "<input type='hidden' name='old' value='{$detail['result']->NAME}'/>" : ""?>
							</div>
							<div class="form-group">
								<label for="input-price">Price*</label>
								<div class="input-group">
									<span class="input-group-addon">Rp</span>
									<input type="number" min="0" name="price" class="form-control" id="input-price" value="<?=isset($detail) ? $detail['result']->PRICE : ""?>" required/></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="form-search-location">Type of Court*</label>
								<select name="type" class="form-control" id="input-type" required>
									<?php
										$tp = "";
										if(isset($detail))
											$tp = $detail['result']->TYPE;
										$this->load->view("frontpage/ajax/form_type", array("type" => $type, "tp" => $tp))
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="input-long">Long Time per Order*</label>
								<select name="long" class="form-control" id="input-long" required>
									<?php
										$hr = 1;
										if(isset($detail))
											$hr = $detail['result']->PER;
									?>
									<option value="0" <?php if($hr==0) echo "selected"?>>per 1/2 Hour</option>
									<option value="1" <?php if($hr==1) echo "selected"?>>per Hour</option>
									<option value="2" <?php if($hr==2) echo "selected"?>>per 2 Hours</option>
									<option value="3" <?php if($hr==3) echo "selected"?>>per 3 Hours</option>
									<option value="4" <?php if($hr==4) echo "selected"?>>per 4 Hours</option>
								</select>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->

			</section><!-- /.Right col -->
			
		<?php if(isset($detail)): ?>
			<section class="col-lg-5">
				<div class="box box-danger">
					<div class="box-header">
						<i class="ion ion-clipboard"></i>
						<h3 class="box-title">Schedule</h3>
						<div class="box-tools pull-right">
							<button type="submit" form="form-schedule" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
						</div>
					</div><!-- /.box-header -->
					<div class="box-body">
						<?php if($this->session->flashdata("error_schedule")) { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("error_schedule")?>
							</div>
						<?php } ?>
						<?php if($this->session->flashdata("success_schedule")) { ?>
							<div class="alert alert-success">
								<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
								<?=$this->session->flashdata("success_schedule")?>
							</div>
						<?php } ?>
						<form action="<?=site_url("vendor/save_court_schedule")?>" method="post" id="form-schedule">
							<div class="box-group" id="accordion">
								
								<input type='hidden' name='name' value='<?=$detail['result']->NAME;?>'/>
								<?php
									$days = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
									for($j=0;$j<7;$j++):
								?>
								
									<div class="panel box-solid">
										<div class="box-header with-border">
											<h4 class="box-title">
												<?=$days[$j]?>
											</h4>
											<div class="box-tools pull-right">
												<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$days[$j]?>" style="color:grey"><i class="fa fa-chevron-down"></i></a>
											</div>
										</div>
										
										<div id="collapse<?=$days[$j]?>" class="panel-collapse collapse<?=$j ? "" : " in"?>">
											<div class="box-body">
												<?php
													$avTimeInDay = array();
													foreach($schedule['result'] as $row) {
														if($row->DAY == substr($days[$j],0,3))
															$avTimeInDay[$row->TIME] = true;
													}
													
													$pembagi = floor(24 / 4);
													for($i=0;$i<24;$i++) {
														if($i == 0)
															echo "<div class='col-md-3'>";
														
														echo "<input type='checkbox' name='time[]'";
														if(isset($avTimeInDay[date("H:i:s", strtotime("$i:00"))]))
															echo " checked='checked'";
														echo " value='". substr($days[$j],0,3) ."_$i:00'/> ". date("H:i", strtotime("$i:00")) . "<br/>";
														
														if(($i+1) % ($pembagi ? $pembagi : 1) == 0) {
															echo "</div>
																		<div class='col-md-3'>";
														}
														if($i == (24 - 1))
															echo "</div>";
													}
												?>
											</div>
										</div>
									</div>
								<?php endfor;?>
							</div>
						</form>
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</section>
		<?php endif;?>
		
		</div>
	</section>
</div>