<body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#">Vendor <b>Play</b>House</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
				<?php if($this->session->flashdata("error_sign_in")) { ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" area-hidden="true">&times;</button>
						<?=$this->session->flashdata("error_sign_in")?>
					</div>
				<?php } ?>
        <form action="<?=site_url("vendor/login_proc")?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="userid" class="form-control" placeholder="ID" required>
            <span class="fa fa-credit-card form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4 col-xs-push-8">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

        <!--<a href="#">I forgot my password</a>-->

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=base_url("assets/js/jquery.js")?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=base_url("assets/js/bootstrap.js")?>"></script>
  </body>