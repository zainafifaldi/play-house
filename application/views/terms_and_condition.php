<div class="modal fade" id="terms-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Terms and Conditions</h4>
			</div>
			<div class="modal-body">
				<fieldset class="terms-field">
					<h1 class="terms-title">TERMS AND CONDITIONS</h1>
					<p>
						This section explains the terms and conditions you are agreeing to when you use playhouseapp.com / Playhouse apps (IOS or Android) online sport booking services.
						Please read these terms and conditions carefully. By using the playhouseapp.com / Playhouse apps (IOS or Android) online sport booking services, you agree to abide by the following terms and conditions.
						If you do not accept any of these terms and conditions, please discontinue your access of this online service immediately.
					</p>

					<ol class="list-number">
						<li>
							<span class="list-title">DEFINITIONS</span>
							<p>
								“We” or “Us” refer to playhouseapp.com / Playhouse apps (IOS or Android) or its affiliate.
								“Facility” refer to playhouseapp.com / Playhouse apps (IOS or Android) online sport booking services is playhouseapp.com / Playhouse apps (IOS or Android) electronic (or online booking and payment) facility which enables customers to make sports court booking and payment from their desired location utilizing internet connection.
								“You” is a term used to address our patrons using the playhouseapp.com / Playhouse apps (IOS or Android) online sport booking services. “Transaction” is referred to as the process of sports court(s) booking and payment booking at playhouseapp.com / Playhouse apps (IOS or Android). “Venue” refer to sports centre that you are able to book sports court.
							</p>
						</li>
						
						<li>
							<span class="list-title">DISCLAIMER</span>
							<p>
								playhouseapp.com / Playhouse apps (IOS or Android) is providing this facility as an alternative mode of making sports court(s) booking and payment and makes no representations or warranties of any kind, express or implied, with respect to this website or the information, content, products or services included in this site including, without limitation, warranties of merchantability and fitness for a particular purpose.
							</p>
							<p>
								In no circumstances shall playhouseapp.com / Playhouse apps or (IOS or Android) or Finebyte Sdn Bhd or any of its officers or employees or be liable for any loss, additional costs or damage howsoever arising suffered as a result of any use of this facility. You understand that by buying playhouseapp.com / Playhouse apps (IOS or Android) credit, you are paying Finebyte Sdn Bhd funds in exchange for credits.
								playhouseapp.com / Playhouse apps (IOS or Android) credits sold are not refundable in any form. playhouseapp.com / Playhouse apps (IOS or Android) credits has no cash values and only usable in playhouseapp.com / Playhouse apps (IOS or Android).
							</p>
							<p>
								We may change these terms from time to time without prior notice. Changes will apply to any subsequent transactions
							</p>
							<p style="font-weight:bold">
								This facility is currently only to be used for making sports court(s) booking and payment only.
							</p>
						</li>
						
						<li>
							<span class="list-title">GENERAL</span>
							<ol class="list-alphabet">
								<li>In order to perform a booking and payment, you will have to complete all the steps mentioned until you are able to print the receipt. We are not responsible for any fail booking or payment.</li>
								<li>You will then be directed to our third party assigned by us to handle payment processing.</li>
								<li>All internet purchases are confirmed purchases and no refunds, exchanges or cancellations will be allowed even if the customers made a request.</li>
								<li>Every transactions are restricted to booking of 1 sports court of a define duration of play only.</li>
								<li>Kindly be noted that the rates and operation hours for each of our venue.  Therefore, we will strictly adhere to the opening & closing hours as well as the Ad-hoc/Walk-In Rate of each venue.</li>
								<li>Fees being offered might be different between playhouseapp.com / Playhouse apps (IOS or Android) with the actual venue.</li>
								<li>Venue has the right to give away your booking if you are late more than 10 minutes even if you have made payment.</li>
								<li>All venue House Rules are applicable for playhouseapp.com / Playhouse apps (IOS or Android) customers.</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">PRIVACY STATEMENT</span>
							<ol class="list-alphabet">
								<li>All playhouseapp.com / Playhouse apps (IOS or Android) transactions are linked to the secured e-GHL facility.</li>
								<li>Upon every successful transaction, customers will be given a booking reference number.  Kindly keep this booking number, and if possible, print out and bring along the paid confirmation receipt. We may also require your identity card to be presented for verification purposes.</li>
							</ol>
						</li>
						
						<li>
							<span class="list-title">CREDIT CARD</span>
							<p>
								By using playhouseapp.com / Playhouse apps (IOS or Android) facility, you consent to the collection and use of your personal information in accordance with this policy. Though the policy has been developed to take account of changes in Internet technology, it is subject to change and any such changes will be notified on this page.
							</p>
							<p>
								Credit card information given is only retained for the purpose of the transaction.
							</p>
						</li>
					</ol>

					<p>
						When you perform any e-Payment transaction from our site, your computer automatically accesses a small piece of code on our server (a 'server-side cookie') which enables the web pages to be displayed correctly. This cookie is not saved on your computer and is only used for the duration of the session.
					</p>
					<p>
						If you are using a web browser set to high security you may experience difficulties using this facility.
					</p>
				</fieldset>
			</div>
		  <div class="modal-footer" style="margin-top:0">
				<button class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		</div>
	</div>
</div>