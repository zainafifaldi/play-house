<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set("Asia/Jakarta");
		$this->basepath = $_SERVER['DOCUMENT_ROOT'] . "/playhouze/";
	}
	
	public function index()
	{
		$this->validate("inverse");
		
		$head = array(
							"title" => "Admin Sign In | PlayHouse"
						);
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/page/login");
	}
	
	public function all_order()
	{
		$this->validate();
		
		$head = array(
							"title" => "Court Order | PlayHouse"
						);
		$header = array(
							"active" => "all_order",
						);
		$body = array(
							"order_list" => $this->Bookmodel->get_all_order(),
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/all_order", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function weekly_order($offset = 0)
	{
		// Minus return index of wednesday
		$minus = date("N") - 3;
		if($minus < 0)
			$minus += 7;
		
		// minusFrom is index of wednesday last week, minusTo is tuesday this week (without offset)
		$minusFrom = $minus + ($offset+1) * 7;
		$minusTo = $minus + $offset * 7 + 1;
		
		$this->validate();
		
		$head = array(
							"title" => "Weekly Venue Book | PlayHouse"
						);
		$header = array(
							"active" => "weekly_order",
						);
		$body = array(
							"order_list" => $this->Bookmodel->get_weekly_order($minusFrom, $minusTo),
							"offset" => $offset,
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/weekly_order", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function weekly_detail($venue, $offset)
	{
		// Minus return index of wednesday
		$minus = date("N") - 3;
		if($minus < 0)
			$minus += 7;
		
		// minusFrom is index of wednesday last week, minusTo is tuesday this week (without offset)
		$minusFrom = $minus + ($offset+1) * 7;
		$minusTo = $minus + $offset * 7 + 1;
		
		$this->validate();
		
		$head = array(
							"title" => "Weekly Venue Book Detail | PlayHouse"
						);
		$header = array(
							"active" => "weekly_order",
						);
		$body = array(
							"order_list" => $this->Bookmodel->get_weekly_order_detail($venue, $minusFrom, $minusTo),
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/weekly_detail", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function frontpage()
	{
		$this->validate();
		
		$head = array(
							"title" => "Setting Front Page | PlayHouse"
						);
		$header = array(
							"active" => "frontpage",
							"venue_list" => $this->Venuemodel->get_all_venue(),
						);
		$body = array(
							"ads" => $this->Bookmodel->search_more("t_ads", array("ACTIVE" => 1)),
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/frontpage", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function add_ads()
	{
		$data['HEADLINE'] = $this->input->post("headline");
		$data['CONTENT'] = $this->input->post("content");
		$data['LINK'] = $this->input->post("link");
		
		$insert = $this->Venuemodel->insert("t_ads", $data);
		if($insert['is_error']) {
			$this->session->set_flashdata("error_add", "Failed to save ads. Please check your data and try again.");
			$this->session->set_flashdata($this->input->post());
		}
		else {
			$this->session->set_flashdata("success_add", "Success save ads.");
		}
		
		redirect("admin/frontpage");
	}
	
	public function close_ads($adsId)
	{
		$data['ACTIVE'] = 0;
		$cond['ADS_ID'] = $adsId;
		
		$this->Venuemodel->update("t_ads", $data, $cond);
		if($insert['is_error'])
			$this->session->set_flashdata("error_list", "Failed to close ads.");
		else
			$this->session->set_flashdata("success_list", "Success close ads.");
		
		redirect("admin/frontpage");
	}
	
	public function vendor()
	{
		$this->validate();
		
		$head = array(
							"title" => "Vendor | PlayHouse"
						);
		$header = array(
							"active" => "vendor",
							"venue_list" => $this->Venuemodel->get_all_venue(),
						);
		$body = array(
							"venue" => $this->Venuemodel->get_all_venue(),
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/vendor", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function add_vendor()
	{
		$this->validate();
		
		$head = array(
							"title" => "Add Vendor | PlayHouse"
						);
		$header = array(
							"active" => "vendor",
							"venue_list" => $this->Venuemodel->get_all_venue(),
						);
		$body = array(
							"location" => $this->Locationmodel->get_all_db_location(),
						);
		$foot = array(
							
						);
		
		$this->load->view("admin/master/head", $head);
		$this->load->view("admin/master/header", $header);
		$this->load->view("admin/page/add_vendor", $body);
		$this->load->view("admin/master/foot");
	}
	
	public function add_vendor_proc()
	{
		$data['NAME'] = $this->input->post("name");
		$data['ADDRESS'] = $this->input->post("address");
		$data['LOCATION_ID'] = $this->input->post("loc");
		
		if($data['LOCATION_ID'] == "new") {
			$loc['COUNTRY'] = $this->input->post("country");
			$loc['CITY'] = $this->input->post("city");
			
			$latlong = preg_replace('/\s+/', '', $this->input->post("lat_long"));	// remove all white space
			$latlong = explode(",", $latlong);
			$loc['MAPS_LAT'] = $latlong[0];
			$loc['MAPS_LNG'] = $latlong[1];
			
			$insert = $this->Venuemodel->insert("t_location", $loc);
			if(!$insert['is_error']) {
				$data['LOCATION_ID'] = $insert['result'];
			}
			else {
				$this->session->set_flashdata("error_add", "Failed to add location.");
				$this->session->set_flashdata($this->input->post());
				redirect("admin/add_vendor");
			}
		}
		
		$insert = $this->Venuemodel->insert("t_venue", $data);
		if($insert['is_error']) {
			$this->session->set_flashdata("error_add", "Failed to save venue. Please check your data and try again.");
			$this->session->set_flashdata($this->input->post());
		}
		else {
			if(!$this->Authmodel->check_admin_username($this->input->post("vendor_username"))) {
				if($this->input->post("vendor_pass") == $this->input->post("vendor_repass")) {
					// Password matched
					
					/* Get Profile Info */
					$user['AUTH'] = "ven";
					$user['VENUE_ID'] = $insert['result'];
					$user['NAME'] = $this->input->post("vendor_name");
					$user['USERNAME'] = $this->input->post("vendor_username");
					$user['PASSWORD'] = md5($this->input->post("vendor_pass"));
					// $user['PASSWORD'] = $this->input->post("vendor_pass");
					
					$insert2 = $this->Authmodel->insert("t_admin", $user);
					if(!$insert['is_error']) {
						$this->session->set_flashdata("success_add", "Success save venue.");
						redirect("admin/vendor");
					}
					else {
						// Failed insert
						$this->session->set_flashdata("error_add", "Failed to save user. Please check your data and try again.");
					}
				}
				else {
					// Password not match
					$this->session->set_flashdata("error_add", "Confirm password doesn't match!");
				}
			}
			else {
				// Username already used
				$this->session->set_flashdata("error_add", "Username is already used by another user. Please try other.");
			}
			
			$cond['VENUE_ID'] = $insert['result'];
			$this->Venuemodel->delete("t_venue", $cond);
		}
		
		$this->session->set_flashdata($this->input->post());
		redirect("admin/add_vendor");
	}
	
	public function login_proc()
	{
		$userid = $this->input->post("userid");
		$password = md5($this->input->post("password"));
		// $password = $this->input->post("password");
		
		if($userid && $password) {
			$sign_in = $this->Authmodel->admin_sign_in_check($userid,$password);
			
			if(!$sign_in['is_error']) {
				// Success
				$data = array(
							"log_user" => true,
							"auth" => "admin",
							"id" => $sign_in['result']->ADMIN_ID,
							"userid" => $sign_in['result']->USERNAME,
							"name" => $sign_in['result']->NAME,
						);
				$this->session->set_userdata($data);
			}
			else {
				// Failed
				$this->session->set_flashdata("error_sign_in", "Wrong authentication!");
			}
		}
		else {
			$this->session->set_flashdata("error_sign_in", "Please fill the form below.");
		}
		
		redirect("admin");
	}
	
	function validate($inverse = "no-inverse")
	{
		if($this->session->userdata("log_user")) {
			if($this->session->userdata("auth") == "admin") {
				if($inverse == "inverse")
					redirect("admin/all_order");
			}
			else {
				if($inverse == "no-inverse")
					redirect("admin");
			}
		}
		else {
			if($inverse == "no-inverse")
				redirect("admin");
		}
	}
	
	public function logout()
	{
		$data = array(
							"log_user" => null,
							"auth" => null,
							"id" => null,
							"userid" => null,
							"name" => null,
						);
		$this->session->unset_userdata($data);
		$this->session->sess_destroy();
		
		redirect("admin");
	}
}