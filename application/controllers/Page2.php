<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set("Asia/Jakarta");
	}
	
	public function index()
	{
		$head = array(
							
						);
		$body = array(
							"location" => $this->Locationmodel->get_all_db_location(),
							"type" => $this->Venuemodel->get_all_type(),
							"venue" => $this->Venuemodel->get_venue_by_loc_type("", ""),
							"court" => $this->Venuemodel->get_venue_with_search_tolerant("", "", "", date("Y-m-d"), (date("H")+1), date("H")+5),
							"courtOptional" => $this->Venuemodel->get_venue_with_search_tolerant("", "", "", "", "", ""),
							"ads" => $this->Bookmodel->search_more("t_ads", array("ACTIVE" => 1)),
						);
		$foot = array(
							
						);
		$title = array(
							"title" => "Welcome | Play House"
						);
		
		$this->load->view('frontpage/master/head', $title);
		$this->load->view('frontpage/master/header', $head);
		$this->load->view('frontpage/page/home', $body);
		$this->load->view('frontpage/master/footer', $foot);
	}
	
	public function order_list()
	{
		if(!$this->session->userdata("log_user"))
			redirect("page");
		if($this->session->userdata("auth") != "user")
			redirect("page");
		
		$head = array(
							
						);
		$body = array(
							"location" => $this->Locationmodel->get_all_db_location(),
							"type" => $this->Venuemodel->get_all_type(),
							"venue" => $this->Venuemodel->get_venue_by_loc_type("", ""),
							"order_list" => $this->Bookmodel->get_order_list_by_user($this->session->userdata("id")),
						);
		$foot = array(
							
						);
		$title = array(
							"title" => "Your Order | Play House"
						);
		
		$this->load->view('frontpage/master/head', $title);
		$this->load->view('frontpage/master/header', $head);
		$this->load->view('frontpage/page/order_list', $body);
		$this->load->view('frontpage/master/footer', $foot);
	}
	
	public function form_get_venue()
	{
		$location = $this->input->get("location");
		$type = $this->input->get("type");
		
		$data = array(
							"venue" => $this->Venuemodel->get_venue_by_loc_type($location, $type),
						);
		
		$this->load->view('frontpage/ajax/form_venue', $data);
	}
	
	public function search_result()
	{
		$location = $this->input->get("loc");
		$type = $this->input->get("tp");
		$venue = $this->input->get("ven");
		$date = $this->input->get("d");
		$range = $this->input->get("rt");
		
		if($date) {
			$date = explode("/", $date);
			$date = "$date[2]-$date[1]-$date[0]";
		}
		
		$start = "";
		$end = "";
		
		if($this->input->get("rt")) {
			$range = explode(",", $range);
			$start = $range[0];
			$end = $range[1];
		}
		
		$head = array(
							
						);
		$body = array(
							"location" => $this->Locationmodel->get_all_db_location(),
							"type" => $this->Venuemodel->get_all_type(),
							"venue" => $this->Venuemodel->get_venue_by_loc_type($location, $type),
							"court_strict" => $this->Venuemodel->get_venue_with_search_strict($location, $type, $venue, $date, $start, $end),
							"court_tolerant" => $this->Venuemodel->get_venue_with_search_tolerant($location, $type, $venue, $date, $start, $end),
							"fill" => $this->input->get(),
						);
		$foot = array(
							
						);
		$title = array(
							"title" => "Search Result | Play House"
						);
		
		$this->load->view('frontpage/master/head', $title);
		$this->load->view('frontpage/master/header', $head);
		$this->load->view('frontpage/page/search_result', $body);
		$this->load->view('frontpage/master/footer', $foot);
	}
	
	public function detail($ven, $tp)
	{
		$date = $this->input->get("d");
		$range = $this->input->get("rt");
		$availableDate = $this->input->get("av");
		
		if($date) {
			$date = explode("/", $date);
			$date = "$date[2]-$date[1]-$date[0]";
		}
		else {
			$date = date("Y-m-d");
		}
		
		if($availableDate) {
			$availableDate = explode("/", $availableDate);
			$availableDate = "$availableDate[2]-$availableDate[1]-$availableDate[0]";
		}
		else {
			$availableDate = date("Y-m-d");
		}
		
		$start = "";
		$end = "";
		
		if($this->input->get("rt")) {
			$range = explode(",", $range);
			$start = $range[0];
			$end = $range[1];
		}
		else {
			$start = (date("H")+1);
			$end = (date("H")+2);
		}
		
		$head = array(
							
						);
		$body = array(
							"venue" => $this->Venuemodel->get_venue_detail($ven),
							"type" => $this->Venuemodel->get_type_detail($tp),
							"court" => $this->Venuemodel->get_list_court_by_venue_type($ven, $tp),
							"gallery" => $this->Venuemodel->get_gallery_by_venue_type($ven, $tp),
							"av" => $this->input->get("av"),
							"date" => $date,
							"from" => $start,
							"to" => $end,
							"availableStrict" => $this->Venuemodel->get_venue_with_search_strict("", $tp, $ven, $date, $start, $end, FALSE),
							"availableStrict2" => $date == $availableDate ? array("is_error" => true) : $this->Venuemodel->get_venue_with_search_strict("", $tp, $ven, $availableDate, $start, $end, FALSE),
							"availableDate" => $this->Venuemodel->get_available_venue_by_date($tp, $ven, $availableDate)
						);
		$foot = array(
							
						);
		
		$venueName = !$body['venue']['is_error'] ? $body['venue']['result']->NAME : "No Venue";
		$title = array(
							"title" => "Venue Detail | " . $venueName
						);
		
		$this->load->view('frontpage/master/head', $title);
		$this->load->view('frontpage/master/header', $head);
		$this->load->view('frontpage/page/venue_detail', $body);
		$this->load->view('frontpage/master/footer', $foot);
	}
	
	public function start_book($scheduleId, $bookDate)
	{
		if($this->session->userdata("log_user")) {
			if($this->session->userdata("auth") == "user") {
				$title = array(
									"title" => "Book Detail | PlayHouse"
								);
				$head = array(
									
								);
				$body = array(
									"location" => $this->Locationmodel->get_all_db_location(),
									"type" => $this->Venuemodel->get_all_type(),
									"venue" => $this->Venuemodel->get_venue_by_loc_type("", ""),
									"date" => $bookDate,
									"scheduleId" => $scheduleId,
									"court" => $this->Venuemodel->get_court_detail_by_schedule_id($scheduleId),
									"availableDate" => $this->Venuemodel->get_available_venue_by_schedule_id($scheduleId, $bookDate)
								);
				$foot = array(
									
								);
				
				$this->load->view('frontpage/master/head', $title);
				$this->load->view('frontpage/master/header', $head);
				$this->load->view('frontpage/page/book', $body);
				$this->load->view('frontpage/master/footer', $foot);
			}
			else {
				$this->signin(site_url("page/start_book/$scheduleId/$bookDate"));
			}
		}
		else {
				$this->signin(site_url("page/start_book/$scheduleId/$bookDate"));
		}
	}
	
	public function book_proc()
	{
		$order = $this->input->post("time");
		$date = $this->input->post("date");
		
		$detail = $this->Venuemodel->get_court_detail_by_schedule_id($order[0]);
		
		$save['RESERVATION'] = $this->generate_reservation_id();
		$save['USER_ID'] = $this->session->userdata("id");
		$save['BOOK_DATE'] = $date;
		$save['BOOK_TIMESTAMP'] = date("Y-m-d H:i:s");
		$save['PRICE'] = $detail['result']->PRICE;
		
		$total = 0;
		
		foreach($order as $scheduleId) {
			$save['SCHEDULE_ID'] = $scheduleId;
			$this->Bookmodel->insert("t_book", $save);
			
			$total += $save['PRICE'];
		}
		
		// $payment_url = 'https://test2pay.ghl.com/IPGSG/Payment.aspx';
		$payment_url = 'https://securepay.e-ghl.com/IPG/Payment.aspx';
		$payment_id = 'SIT';
		$payment_pass = 'sit12345';
		$payment_timeout = 780;
		$total = $total * 0.00031;
		
		$data = array(
								"TransactionType"     => "SALE",
								"PymtMethod"          => "ANY",
								"ServiceID"           => $payment_id,
								"PaymentID"           => "PLAYHOUSEDEV".$save['RESERVATION'],
								"OrderNumber"         => $save['RESERVATION'],
								"PaymentDesc"         => "PlayHouseDev.byFineStudio.com Payment #". $save['RESERVATION'],
								"MerchantName"        => "PlayHouseDev.byFineStudio.com",
								"MerchantReturnURL"   => site_url("page/book_completed"),
								"MerchantCallbackURL" => site_url("page/book_completed/0"),
								"Amount"              => number_format($total, 2, '.', ''),
								"CurrencyCode"        => "MYR",
								"CustIP"              => NULL,
								"CustName"            => $this->session->userdata("name"),
								"CustEmail"           => $this->session->userdata("email"),
								"CustPhone"           => $this->session->userdata("phone"),
								"HashValue"           => NULL,
								"MerchantTermsURL"    => base_url("assets/terms.txt"),
								"LanguageCode"        => "en",
								"PageTimeout"         => $payment_timeout,
						);
		$data["CustIP"] = $_SERVER["REMOTE_ADDR"];
		if(strpos($data["CustIP"], ":") !== FALSE)
				$data["CustIP"] = "127.0.0.1";
		$data["HashValue"] = hash("sha256", $payment_pass ."{$data['ServiceID']}{$data['PaymentID']}{$data['MerchantReturnURL']}{$data['MerchantCallbackURL']}{$data['Amount']}{$data['CurrencyCode']}{$data['CustIP']}{$data['PageTimeout']}");
		redirect($payment_url ."?".http_build_query($data));
	}
	
	public function generate_reservation_id()
	{
		$last_id = $this->Bookmodel->get_last_reservation_id();
		$new_id = "";
		
		if(!$last_id['is_error']) {
			$id = $last_id['result']->RESERVATION;
			$new_index = (int)$id + 1;
			
			if($new_index < 10) {
				$new_id .= "0000000". $new_index;
			}else if($new_index >= 10 && $new_index < 100) {
				$new_id .= "000000". $new_index;
			}else if($new_index >= 100 && $new_index < 1000) {
				$new_id .= "00000". $new_index;
			}else if($new_index >= 1000 && $new_index < 10000) {
				$new_id .= "0000". $new_index;
			}else if($new_index >= 10000 && $new_index < 100000) {
				$new_id .= "000". $new_index;
			}else if($new_index >= 100000 && $new_index < 1000000) {
				$new_id .= "00". $new_index;
			}else if($new_index >= 1000000 && $new_index < 10000000) {
				$new_id .= "0". $new_index;
			}else {
				$new_id .= $new_index;
			}
		}
		else {
			$new_id .= "00000001";
		}
		
		return $new_id;
	}
	
	public function book_completed($direct = 1)
	{
		// $payment_url = 'https://test2pay.ghl.com/IPGSG/Payment.aspx';
		$payment_url = 'https://securepay.e-ghl.com/IPG/Payment.aspx';
		$payment_id = 'SIT';
		$payment_pass = 'sit12345';
		$payment_timeout = 780;
		
		$HashValue = hash("sha256", $payment_pass."{$this->input->post("TxnID")}{$this->input->post("ServiceID")}{$this->input->post("PaymentID")}{$this->input->post("TxnStatus")}{$this->input->post("Amount")}{$this->input->post("CurrencyCode")}{$this->input->post("AuthCode")}");
		if($this->input->post("HashValue") != $HashValue)
		{
			$this->session->set_flashdata("error", "Hash value is not valid. Please contact admin.");
		}
		
		$cond['RESERVATION'] = substr($this->input->post("PaymentID"), 12);
		
		if($this->input->post("TxnStatus") == 1)
		{
			// Transaction Failed
			$update = $this->Bookmodel->update("t_book", array(
																											"PAYMENT_METHOD"    => $this->input->post("PymtMethod"),
																											"PAYMENT_ID"        => $this->input->post("TxnID"),
																											"PAYMENT_BANK"      => $this->input->post("IssuingBank"),
																											"PAYMENT_AUTHCODE"  => $this->input->post("AuthCode"),
																											"PAYMENT_MESSAGE"   => $this->input->post("TxnMessage"),
																											"PAYMENT_TOKENTYPE" => $this->input->post("TokenType"),
																											"PAYMENT_TOKEN"     => $this->input->post("Token"),
																											"LOCK_STATE"  	    => 0
																									), $cond);
			
			$this->session->set_flashdata("error", "Transaction Failed. ". $this->input->post("TxnMessage") .". Please check your payment data and try again. If this happen again, please contact admin.");
			// print_r($this->input->post());
			// exit;
		}
		else {
			// Transaction Success
			$update = $this->Bookmodel->update("t_book", array(
																											"PAYMENT_METHOD"    => $this->input->post("PymtMethod"),
																											"PAYMENT_ID"        => $this->input->post("TxnID"),
																											"PAYMENT_BANK"      => $this->input->post("IssuingBank"),
																											"PAYMENT_AUTHCODE"  => $this->input->post("AuthCode"),
																											"PAYMENT_MESSAGE"   => $this->input->post("TxnMessage"),
																											"PAYMENT_TOKENTYPE" => $this->input->post("TokenType"),
																											"PAYMENT_TOKEN"     => $this->input->post("Token"),
																											"LOCK_STATE"  	    => 2
																									), $cond);
			if(!$update['is_error'])
			{
				$this->session->set_flashdata("success", "Payment success! Thank you for your choosing PlayHouse.");
			}
			else
			{
				$this->session->set_flashdata("error", "Database error. Please contact our administrator and give your payment bill. We will updated your booking manually.");
			}
		}
		
		redirect("page");
	}
	
	public function signin($redirect = "")
	{
		if($this->session->userdata("log_user")) {
			if($this->session->userdata("auth") == "user") {
				redirect("page");
			}
		}
		
		$title = array(
							"title" => "Sign In | PlayHouse"
						);
		$head = array(
							
						);
		$body = array(
							"redirect" => $redirect
						);
		$foot = array(
							
						);
		
		$this->load->view("frontpage/master/head", $title);
		$this->load->view("frontpage/master/header", $head);
		$this->load->view("frontpage/page/login", $body);
		$this->load->view("frontpage/master/footer", $foot);
	}
	
	public function login_proc()
	{
		$username = $this->input->post("login_username");
		$password = md5($this->input->post("password"));
		// $password = $this->input->post("password");
		$redirect = $this->input->post("redirect");
		
		if($username && $password) {
			$sign_in = $this->Authmodel->user_sign_in_check($username,$password);
			
			if(!$sign_in['is_error']) {
				// Success
				$data = array(
							"log_user" => true,
							"auth" => "user",
							"id" => $sign_in['result']->USER_ID,
							"name" => $sign_in['result']->FIRST_NAME . " " . $sign_in['result']->LAST_NAME,
							"first_name" => $sign_in['result']->FIRST_NAME,
							"last_name" => $sign_in['result']->LAST_NAME,
							"email" => $sign_in['result']->EMAIL,
							"phone" => $sign_in['result']->PHONE,
						);
				$this->session->set_userdata($data);
				$this->session->set_flashdata("success", "Your are logged in.");
		
				if($redirect)
					redirect($redirect);
				else
					redirect("page");
			}
			else {
				// Failed
				$this->session->set_flashdata("login_error", "Wrong authentication!");
				$this->session->set_flashdata($this->input->post());
			}
		}
		else {
			$this->session->set_flashdata("login_error", "Please fill the form below.");
			$this->session->set_flashdata($this->input->post());
		}
		
		if($redirect)
			redirect($redirect);
		else
			redirect("page/signin");
	}
	
	public function register_proc()
	{
		/* Set Rules */
		$message = array("required" => "Please fill form correctly");
		
		$this->form_validation->set_rules('first_name', "Name", 'required');
		$this->form_validation->set_rules('last_name', "Name", 'required');
		$this->form_validation->set_rules('username', "Username", 'required');
		$this->form_validation->set_rules('identity', "Address", 'required');
		$this->form_validation->set_rules('email', "Email", 'required');
		$this->form_validation->set_rules('phone', "Phone", 'required');
		$this->form_validation->set_rules('password', "Password", 'required');
		$this->form_validation->set_rules('repassword', "Re-Password", 'required');
		$this->form_validation->set_rules('terms', "Terms and Condition", 'required');
		/** Set rules */
		
		$redirect = $this->input->post("redirect");
		
		if($this->form_validation->run() == TRUE) {
			if(!$this->Authmodel->check_username($this->input->post("username"))) {
				if($this->input->post("password") == $this->input->post("repassword")) {
					// Password matched
					
					/* Get Profile Info */
					$member['FIRST_NAME'] = $this->input->post("first_name");
					$member['LAST_NAME'] = $this->input->post("last_name");
					$member['USERNAME'] = $this->input->post("username");
					$member['IDENTITY'] = $this->input->post("identity");
					$member['EMAIL'] = $this->input->post("email");
					$member['PHONE'] = $this->input->post("phone");
					$member['PASSWORD'] = md5($this->input->post("password"));
					// $member['PASSWORD'] = $this->input->post("password");
					
					$insert = $this->Authmodel->insert("t_user", $member);
					if(!$insert['is_error']) {
						$data = array(
									"log_user" => true,
									"auth" => "user",
									"id" => $insert['result'],
									"name" => $member['FIRST_NAME'] . " " . $member['LAST_NAME'],
									"first_name" => $member['FIRST_NAME'],
									"last_name" => $member['LAST_NAME'],
									"email" => $member['EMAIL'],
									"phone" => $member['PHONE'],
								);
						$this->session->set_userdata($data);
						$this->session->set_flashdata("success", "Your register success");
						
						$this->send_user_mail($member, $this->input->post("password"));
						
						if($redirect)
							redirect($redirect);
						else
							redirect("page");
					}
					else {
						// Failed insert
						$this->session->set_flashdata("register_error", "Your register failed. Please contact admin.");
						$this->session->set_flashdata($this->input->post());
					}
				}
				else {
					// Password not match
					$this->session->set_flashdata("register_error", "Confirm password doesn't match!");
					$this->session->set_flashdata($this->input->post());
				}
			}
			else {
				// Username already used
				$this->session->set_flashdata("register_error", "Username is already used by another member. Please try other.");
				$this->session->set_flashdata($this->input->post());
			}
		}
		else {
			// False filling form
			$this->session->set_flashdata("register_error", "Please fill form correctly.");
			$this->session->set_flashdata($this->input->post());
		}
		
		if($redirect)
			redirect($redirect);
		else
			redirect("page/signin");
	}
	
	private function send_user_mail($data, $password){
		$config = array(
			'protocol' => 'mail',
			'smtp_host' => 'azzamtrade.com',
			'smtp_port' => 465,
			'smtp_user' => 'no-reply@azzamtrade.com',// your mail name
			'smtp_pass' => '123qweasdzxc',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'wordwrap' => TRUE
		);
		
		// $config = array(
			// 'protocol' => 'mail',
			// 'smtp_host' => 'byfinestudio.com',
			// 'smtp_port' => 465,
			// 'smtp_user' => 'no-reply@byfinestudio.com',// your mail name
			// 'smtp_pass' => 'byfinestudio1qaz',
			// 'mailtype'  => 'html', 
			// 'charset'   => 'iso-8859-1',
			// 'wordwrap' => TRUE
		// );
		
		$this->load->library('email', $config);
		
		$this->email->from('no-reply@azzamtrade.com', 'PlayHouse');//your mail address and name
		$this->email->to($data['EMAIL']); //receiver mail

		$this->email->subject('Welcome to Play House!');
		
		$message = <<<EOF
Welcome to Playhouse!


Thank you for registering to PlayHouse Apps. This is your account and your sign in information:

First Name		 : $data[FIRST_NAME]
Last Name		: $data[LAST_NAME]
Identity Number		  : $data[IDENTITY]
Phone Number		: $data[PHONE]
Username		: $data[USERNAME]
Password		 : $password


Please keep this data save.
Thank you,


PlayHouse
EOF;
		
		$this->email->message($message);
		// echo $message;
		if($this->email->send()){
			// echo 'Email sent.'; 
		}
		else{
			print_r($this->email->print_debugger());  
		}
	}
	
	public function signout()
	{
		$data = array(
							"log_user" => null,
							"auth" => null,
							"id" => null,
							"name" => null,
							"first_name" => null,
							"last_name" => null,
							"email" => null,
							"phone" => null,
						);
		$this->session->unset_userdata($data);
		$this->session->sess_destroy();
		
		redirect("page");
	}
	
	// public function update_table()
	// {
		// $this->load->database();
		// $this->db->query("ALTER TABLE `t_venue`
// ADD COLUMN `CREATED_DATE`  datetime NOT NULL AFTER `PUBLISH`;

// ");
	// }
}
