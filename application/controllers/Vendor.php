<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor extends CI_Controller
{
	public $basepath;
	
	public function __construct()
	{
		parent::__construct();
		
		date_default_timezone_set("Asia/Jakarta");
		$this->basepath = $_SERVER['DOCUMENT_ROOT'] . "/playhouze/";
	}
	
	public function index()
	{
		$this->validate("inverse");
		
		$head = array(
							"title" => "Vendor Sign In | PlayHouse"
						);
		$this->load->view("vendor/master/head", $head);
		$this->load->view("vendor/page/login");
	}
	
	public function order()
	{
		$this->validate();
		
		$head = array(
							"title" => "Venue Order | PlayHouse"
						);
		$header = array(
							"active" => "order",
							"court_list" => $this->Venuemodel->get_list_court_by_venue($this->session->userdata("venue")),
						);
		$body = array(
							"pending_order_list" => $this->Bookmodel->get_plan_order_by_venue($this->session->userdata("venue"), 1),
							"success_order_list" => $this->Bookmodel->get_plan_order_by_venue($this->session->userdata("venue"), 2),
							"history_order_list" => $this->Bookmodel->get_history_order_by_venue($this->session->userdata("venue")),
						);
		$foot = array(
						
						);
		
		$this->load->view("vendor/master/head", $head);
		$this->load->view("vendor/master/header", $header);
		$this->load->view("vendor/page/order", $body);
		$this->load->view("vendor/master/foot", $foot);
	}
	
	public function set_done($reservation)
	{
		$data['LOCK_STATE'] = 2;
		$cond['RESERVATION'] = $reservation;
		
		$update = $this->Bookmodel->update("t_book", $data, $cond);
		if($update['is_error'])
			$this->session->set_flashdata("set_done_error", "Error set order to done. Please try again later.");
		else
			$this->session->set_flashdata("set_done_success", "Success set order to done");
		
		redirect("vendor/order");
	}
	
	public function venue()
	{
		$head = array(
							"title" => "Venue Editor | PlayHouse"
						);
		$header = array(
							"active" => "venue",
							"court_list" => $this->Venuemodel->get_list_court_by_venue($this->session->userdata("venue")),
						);
		$body = array(
							"location" => $this->Locationmodel->get_all_db_location(),
							"venue" => $this->Venuemodel->get_venue_detail($this->session->userdata("venue")),
						);
		$foot = array(
						
						);
		
		$this->load->view("vendor/master/head", $head);
		$this->load->view("vendor/master/header", $header);
		$this->load->view("vendor/page/venue", $body);
		$this->load->view("vendor/master/foot", $foot);
	}
	
	public function save_ven_desc()
	{
		$data['DESCRIPTION'] = $this->input->post("desc");
		$condition['VENUE_ID'] = $this->session->userdata("venue");
		
		$update = $this->Venuemodel->update("t_venue", $data, $condition);
		if($update['is_error'])
			$this->session->set_flashdata("error_desc", "Failed to save description");
		else
			$this->session->set_flashdata("success_desc", "Success save description");
		
		redirect("vendor/venue");
	}
	
	public function save_ven_detail()
	{
		$data['NAME'] = $this->input->post("name");
		$data['ADDRESS'] = $this->input->post("address");
		$data['LOCATION_ID'] = $this->input->post("loc");
		$data['MAPS_LAT'] = $this->input->post("lat");
		$data['MAPS_LNG'] = $this->input->post("lng");
		$condition['VENUE_ID'] = $this->session->userdata("venue");
		
		$update = $this->Venuemodel->update("t_venue", $data, $condition);
		if($update['is_error'])
			$this->session->set_flashdata("error_detail", "Failed to save detail");
		else
			$this->session->set_flashdata("success_detail", "Success save detail");
		
		redirect("vendor/venue");
	}
	
	public function court($courtName)
	{
		$courtName = urldecode($courtName);
		
		$head = array(
							"title" => "Court Editor | PlayHouse",
						);
		$header = array(
							"active" => "court",
							"court_list" => $this->Venuemodel->get_list_court_by_venue($this->session->userdata("venue")),
						);
		$body = array(
							"edit_type" => "Editor",
							"type" => $this->Venuemodel->get_all_type(),
							"detail" => $this->Venuemodel->get_court_detail($this->session->userdata("venue"), $courtName),
							"gallery" => $this->Venuemodel->get_gallery_by_court($this->session->userdata("venue"), $courtName),
							"schedule" => $this->Venuemodel->get_court_schedule_list($this->session->userdata("venue"), $courtName),
						);
		$foot = array(
							
						);
		
		$this->load->view("vendor/master/head", $head);
		$this->load->view("vendor/master/header", $header);
		$this->load->view("vendor/page/court", $body);
		$this->load->view("vendor/master/foot", $foot);
	}
	
	public function add_court()
	{
		$head = array(
							"title" => "Court Editor | PlayHouse",
						);
		$header = array(
							"active" => "add_court",
							"court_list" => $this->Venuemodel->get_list_court_by_venue($this->session->userdata("venue")),
						);
		$body = array(
							"edit_type" => "Add New",
							"type" => $this->Venuemodel->get_all_type(),
						);
		$foot = array(
						
						);
		
		$this->load->view("vendor/master/head", $head);
		$this->load->view("vendor/master/header", $header);
		$this->load->view("vendor/page/court", $body);
		$this->load->view("vendor/master/foot", $foot);
	}
	
	public function save_court_detail()
	{
		$data['NAME'] = $this->input->post("name");
		$data['PRICE'] = $this->input->post("price");
		$data['TYPE'] = $this->input->post("type");
		$data['PER'] = $this->input->post("long");
		$data['OPEN_RANGE'] = $this->input->post("rt");
		
		$action = $this->input->post("act");
		$do = array();
		
		if($action == "1") {	// Add Court
			$data['VENUE_ID'] = $this->session->userdata("venue");
			
			$do = $this->Venuemodel->insert("t_court", $data);
		}
		else if($action == "2") { // Update Court
			$condition['VENUE_ID'] = $this->session->userdata("venue");
			$condition['NAME'] = $this->input->post("old");
			
			$do = $this->Venuemodel->update("t_court", $data, $condition);
		}
		
		if($do['is_error'])
			$this->session->set_flashdata("error_detail", "Failed to save detail");
		else
			$this->session->set_flashdata("success_detail", "Success save detail");
		
		if($do['is_error'] && $action == "1")
			redirect("vendor/add_court");
		else
			redirect("vendor/court/". $data['NAME']);
	}
	
	public function save_court_schedule()
	{
		$schedule = $this->input->post("time");
		$name = $this->input->post("name");
		
		$error = false;
		
		foreach($schedule as $time) {
			$exp = explode("_", $time);
			
			$cond['VENUE_ID'] = $this->session->userdata("venue");
			$cond['COURT'] = $name;
			$cond['TIME'] = $exp[1];
			$cond['DAY'] = $exp[0];
			$check = $this->Bookmodel->search_one("t_schedule", $cond);
			
			$data2['VENUE_ID'] = $this->session->userdata("venue");
			$data2['COURT'] = $name;
			$data2['TIME'] = $exp[1];
			$data2['DAY'] = $exp[0];
			
			if($check['is_error']) {	// No data
				$ins = $this->Bookmodel->insert("t_schedule", $data2);
				if($ins['is_error'])
					$error = true;
			}
			else {
				$cond['SCHEDULE_ID'] = $check['result']->SCHEDULE_ID;
				$upd = $this->Bookmodel->update("t_schedule", $data2, $cond);
				
				unset($cond['SCHEDULE_ID']);
			}
		}
		
		if($error)
			$this->session->set_flashdata("error_schedule", "Failed to save schedule.");
		else
			$this->session->set_flashdata("success_schedule", "Success save schedule");
		
		redirect("vendor/court/$name");
	}
	
	public function generate_schedule($range, $per)
	{
		$range = explode(",", $range);
		$open = $range[0];
		$close = $range[1];
		
		$plus = (int)$per;
		$time = array();
		
		if($plus == 0) {
			for($i=$open; $i<$close; $i++) {
					$time[] = "$i:00";
					$time[] = "$i:30";
			}
		}
		else {
			for($i=$open; $i<$close; $i+=$plus) {
				$time[] = "$i:00";
			}
		}
		
		return $time;
	}
	
	public function save_court_gallery()
	{
		for($i=0; $i<count($_FILES['photo']['name']); $i++) {
			$tmpFilePath = $_FILES['photo']['tmp_name'][$i];

			if ($tmpFilePath != ""){
				// Save to database
				$data['VENUE_ID'] = $this->session->userdata("venue");
				$data['COURT'] = $this->input->post("court");
				
				$insert = $this->Venuemodel->insert("t_gallery_court", $data);
				if(!$insert['is_error']) {
					$photoName = $insert['result'] .'_'. $_FILES['photo']['name'][$i];
					$newFilePath = $this->basepath .'photo/gallery/'. $photoName;
					
					//Upload the file into the temp dir
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
						$updt['PHOTO'] = $photoName;
						$condition['GALLERY_ID'] = $insert['result'];
						$update = $this->Venuemodel->update("t_gallery_court", $updt, $condition);
						
						$this->session->set_flashdata("success_gallery", "Success upload file");
					}
					else {
						$condition['GALLERY_ID'] = $insert['result'];
						$delete = $this->Venuemodel->delete("t_gallery_court", $condition);
						$this->session->set_flashdata("error_gallery", "Failed to upload file");
					}
				}
				else {
						$this->session->set_flashdata("error_gallery", "Failed to save file");
				}
			}
		}
		
		redirect("vendor/court/". $this->input->post("court"));
	}
	
	public function set_main_photo($courtName, $galleryId)
	{
		$data['PHOTO'] = urldecode($galleryId);
		
		$condition['NAME'] = urldecode($courtName);
		$condition['VENUE_ID'] = $this->session->userdata("venue");
		
		$update = $this->Venuemodel->update("t_court", $data, $condition);
		
		if($update['is_error'])
			$this->session->set_flashdata("error_gallery", "Failed to set main photo");
		else
			$this->session->set_flashdata("success_gallery", "Success set main photo");
		
		redirect("vendor/court/". $courtName);
	}
	
	public function delete_photo($courtName, $galleryId)
	{
		$data['VENUE_ID'] = $this->session->userdata("venue") * (-1);
		$condition['GALLERY_ID'] = urldecode($galleryId);
		
		$update = $this->Venuemodel->update("t_gallery_court", $data, $condition);
		
		if($update['is_error'])
			$this->session->set_flashdata("error_gallery", "Failed to delete photo");
		else
			$this->session->set_flashdata("success_gallery", "Success delete photo");
		
		redirect("vendor/court/". $courtName);
	}
	
	public function login_proc()
	{
		$userid = $this->input->post("userid");
		$password = md5($this->input->post("password"));
		// $password = $this->input->post("password");
		
		if($userid && $password) {
			$sign_in = $this->Authmodel->vendor_sign_in_check($userid,$password);
			
			if(!$sign_in['is_error']) {
				if($sign_in['result']->VENUE_ID != "") {
					// Success
					$data = array(
								"log_user" => true,
								"auth" => "vendor",
								"id" => $sign_in['result']->ADMIN_ID,
								"userid" => $sign_in['result']->USERNAME,
								"name" => $sign_in['result']->NAME,
								"venue" => $sign_in['result']->VENUE_ID,
							);
					$this->session->set_userdata($data);
				}
				else {
					// Failed
					$this->session->set_flashdata("error_sign_in", "Bad venue relation.");
				}
			}
			else {
				// Failed
				$this->session->set_flashdata("error_sign_in", "Wrong authentication!");
			}
		}
		else {
			$this->session->set_flashdata("error_sign_in", "Please fill the form below.");
		}
		
		redirect("vendor");
	}
	
	function validate($inverse = "no-inverse")
	{
		if($this->session->userdata("log_user")) {
			if($this->session->userdata("auth") == "vendor") {
				if($inverse == "inverse")
					redirect("vendor/order");
			}
			else {
				if($inverse == "no-inverse")
					redirect("vendor");
			}
		}
		else {
			if($inverse == "no-inverse")
				redirect("vendor");
		}
	}
	
	public function logout()
	{
		$data = array(
							"log_user" => null,
							"auth" => null,
							"id" => null,
							"userid" => null,
							"name" => null,
							"venue" => null,
						);
		$this->session->unset_userdata($data);
		$this->session->sess_destroy();
		
		redirect("vendor");
	}
}